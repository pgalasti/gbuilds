#include "TestWindow.h"

TestWindow::TestWindow(uint32 width, uint32 height, const char8* pszTitle) : m_IsClosed(false)
{
	SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	m_pWindow = SDL_CreateWindow(pszTitle, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, (int)width, (int)height, SDL_WINDOW_OPENGL);
	m_glContext = SDL_GL_CreateContext(m_pWindow);

	GLenum status = glewInit();
	if (status != GLEW_OK)
	{
		std::cerr << "Glew failed to initialize!" << std::endl;
	}
}

TestWindow::~TestWindow()
{
	SDL_GL_DeleteContext(m_glContext);
	SDL_DestroyWindow(m_pWindow);
	SDL_Quit();
}

void TestWindow::Clear(float32 r, float32 g, float32 b, float32 a)
{
	glClearColor(r, g, b, a);
	glClear(GL_COLOR_BUFFER_BIT);
}

void TestWindow::Update()
{
	SDL_GL_SwapWindow(m_pWindow);

	SDL_Event sdlEvent;

	while (SDL_PollEvent(&sdlEvent))
	{
		if (sdlEvent.type == SDL_QUIT)
			m_IsClosed = true;
	}
}