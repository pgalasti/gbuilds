#ifndef OPENGL_ENGINE_H
#define OPENGL_ENGINE_H

#include "StdHeader.h"

#include <string> 
#include <map>
#include <unordered_map>

#include <GL/glew.h>

#include "IGraphicsEngine.h"
#include "OpenGLEngineTypes.h"
#include "ShaderTypes.h"
#include "GFileApi.h"
#include "GColor.h"
#include "OpenGLMesh.h"
#include "OpenGLTexture.h"
#include <memory>
#include "IDGenerator.h"
#include "GCamera.h"

namespace GApi {
	namespace Graphics {
		namespace OpenGL {

			using namespace GApi::Graphics;
			using namespace GApi::IO;
			using namespace GApi::Util;

			typedef std::shared_ptr<OpenGLMesh> OpenGLMeshPtr;
			
			typedef std::shared_ptr<DDSTexture> DDSTexturePtr;
			typedef std::shared_ptr<Camera> CameraPtr;

			typedef std::unordered_map<std::string, GLuint> ShaderProgramMap;
			typedef std::multimap<GLuint, GLuint> ProgramToShaderMap;
			typedef std::unordered_map<MeshID, GLuint> MeshToProgramMap;
			typedef std::unordered_map<MeshID, OpenGLMeshPtr> MeshMap;
			typedef std::unordered_map<MeshID, Transform> MeshTransformationMap;
			typedef std::unordered_map<TextureID, ITexturePtr> TextureMap;
			typedef std::unordered_map<MeshID, TextureID> MeshTextureMap;
			

			class DllExport OpenGLEngine : implements IGraphicsEngine
			{
			public:

				OpenGLEngine();
				virtual ~OpenGLEngine() {}

				// Options
				virtual BOOL SetOptions(const GraphicsEngineOptions& options) override;
				virtual BOOL SetWindowHandle(void* ptr) override;

				// Initialize/Shutdown
				virtual BOOL Startup() override;
				virtual BOOL Shutdown() override;

				// Rendering
				virtual BOOL Render() override;
				virtual BOOL ClearScreen(const RGBAColor& clearColor) override;

				// Shader Program Functions
				virtual BOOL CreateShaderProgram(const char8* pszProgramName) override;
				virtual BOOL DeleteShaderProgram(const char8* pszProgramName) override;
				virtual BOOL AttachShaderToProgram(const char8* pszShaderProgramName, const char8* pszProgramName, ShaderTypesEnum shaderType) override;

				// Modeling
				virtual BOOL RegisterMesh(Mesh& mesh, MeshID& meshID) override;
				virtual BOOL DeregisterMesh(const MeshID meshID) override;
				virtual BOOL CopyMesh(const MeshID meshID, MeshID& newMesh) override;
				virtual BOOL BindShaderToMesh(const char8* pszProgramName, const MeshID meshID) override;
				virtual BOOL TransformMesh(const MeshID meshID, const Transform& transformation) override;

				// Texture
				virtual BOOL RegisterTexture(std::string resourceName, TextureID& textureID) override;
				virtual BOOL DeregisterTexture(const TextureID textureID) override;
				virtual BOOL BindTextureToMesh(const TextureID textureID, const MeshID meshID) override;

				// Camera
				virtual std::shared_ptr<Camera> GetCamera() override;

				inline virtual std::string GetEngineDescription() const override;

			protected:

				GraphicsEngineOptions m_Options;
				bool m_bOptionsAreSet;

				// Graphics camera
				CameraPtr m_pCamera;

				// Shader path resolver
				FileResolver m_ShaderPathResolver;
				FileResolver m_ResourcePathResolver;

				// Maps a name to a shader program
				ShaderProgramMap m_ShaderProgramMap;
				// Maps a program to one or more shaders
				ProgramToShaderMap m_ProgramToShaderMap;
				// Maps a mesh to one shader program
				MeshToProgramMap m_MeshToProgramMap;
				// MeshID to mesh pointers
				MeshMap m_MeshMap;
				// Maps a mesh to a transformation
				MeshTransformationMap m_MeshTransformationMap;

				TextureMap m_TextureMap;

				MeshTextureMap m_MeshToTextureMap;

				IDGenerator<MeshID>* m_pMeshIDGenerator;
				IDGenerator<TextureID>* m_pTextureIDGenerator;
			};

		}
	}
}

#endif