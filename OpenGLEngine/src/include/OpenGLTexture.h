#ifndef OPENGL_TEXTURE_H
#define OPENGL_TEXTURE_H

#include "GTexture.h"

#include "nv_dds.h"

namespace GApi {
	namespace Graphics {
		namespace OpenGL {

			class DllExport OpenGLTextureImpl : implements GApi::Graphics::ITexture
			{
			public:
				OpenGLTextureImpl(GLuint textureObjectID) : m_TextureObjectID(textureObjectID) {}
				virtual ~OpenGLTextureImpl(){};

				virtual BOOL LoadTexture(const char8* pszFileName) = VIRTUAL;
				virtual BOOL ApplyTexture() = VIRTUAL;
				virtual void FreeTexture() = VIRTUAL;

			protected:
				GLuint m_TextureObjectID;
			};

			class DllExport DDSTexture : public OpenGLTextureImpl
			{
			public:
				DDSTexture(GLuint textureObjectID);
				virtual ~DDSTexture();

				virtual BOOL LoadTexture(const char8* pszFileName) override;
				virtual BOOL ApplyTexture() override;
				virtual void FreeTexture() override;

			protected:
				BOOL LoadDDS(const char8* pszFileName);
				nv_dds::CDDSImage m_DDSImage;
			};

			class DllExport JpegTexture : public OpenGLTextureImpl
			{
			public:
				JpegTexture(GLuint textureObjectID);
				virtual ~JpegTexture();

				virtual BOOL LoadTexture(const char8* pszFileName) override;
				virtual BOOL ApplyTexture() override;
				virtual void FreeTexture() override;

			protected:
				BOOL LoadJpeg(const char8* pszFileName);

			};
		}
	}
}

#endif