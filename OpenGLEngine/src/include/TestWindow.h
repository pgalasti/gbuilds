#include "StdHeader.h"

#include<iostream>
#include<string>
#include <SDL.h>
#include <GL/glew.h>


class DllExport TestWindow 
{

public:

	TestWindow(uint32 width, uint32 height, const char8* pszTitle);

	virtual ~TestWindow();

	void Clear(float32 r, float32 g, float32 b, float32 a);

	void Update();

	bool IsClosed()
	{
		return m_IsClosed;
	}

private:

	SDL_Window* m_pWindow;
	SDL_GLContext m_glContext;

	std::string m_Title;
	bool m_IsClosed;
};

