#ifndef OPENGL_UTILS_H
#define OPENGL_UTILS_H

#include "StdHeader.h"

#include <string>

#include <GL/glew.h>

namespace GApi {
	namespace Graphics {
		namespace OpenGL {
			namespace Utility {


				DllExport BOOL CheckShaderError(GLuint shader, std::string& errorMessage);
				DllExport BOOL CheckProgramError(GLuint program, std::string& errorMessage);


			}
		}
	}
}

#endif