#ifndef OPENGL_CONVERSIONS_H
#define OPENGL_CONVERSIONS_H

#include "StdHeader.h"

#include <GL/glew.h>
#include "GColor.h"
#include "GMath.h"

namespace GApi {
	namespace Graphics {
		namespace OpenGL {

			using namespace GApi::Graphics;
			using namespace GApi::GMath;

			void ToGLColor(const RGColor& color, GLclampf* pColor)
			{
				pColor[0] = Clamp<GLclampf>(color.r, 0.0, 1.0);
				pColor[1] = Clamp<GLclampf>(color.g, 0.0, 1.0);
			}

			void ToGLColor(const RGBColor& color, GLclampf* pColor)
			{
				pColor[0] = Clamp<GLclampf>(color.r, 0.0, 1.0);
				pColor[1] = Clamp<GLclampf>(color.g, 0.0, 1.0);
				pColor[2] = Clamp<GLclampf>(color.b, 0.0, 1.0);
			}

			void ToGLColor(const RGBAColor& color, GLclampf* pColor)
			{
				pColor[0] = Clamp<GLclampf>(color.r, 0.0, 1.0);
				pColor[1] = Clamp<GLclampf>(color.g, 0.0, 1.0);
				pColor[2] = Clamp<GLclampf>(color.b, 0.0, 1.0);
				pColor[3] = Clamp<GLclampf>(color.a, 0.0, 1.0);
			}
		}
	}
}

#endif