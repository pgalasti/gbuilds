#ifndef OPENGL_MESH_H
#define OPENGL_MESH__H

#include "StdHeader.h"
#include "GMesh.h"

#include <GL/glew.h>

namespace GApi {
	namespace Graphics {
		namespace OpenGL {

			class DllExport OpenGLMesh
			{
			public:
				OpenGLMesh(Mesh& mesh);
				~OpenGLMesh();

				GLuint GetVertexArrayObject() const { return m_VertexArrayObject; }
				GLuint GetVertexArrayBuffer() const { return m_VertexArrayBuffer; }

				uint32 GetVertexCount() const { return m_VertexCount; }
			protected:
				GLuint m_VertexArrayObject;
				GLuint m_VertexArrayBuffer;

				uint32 m_VertexCount;
			};

		}
	}
}

#endif