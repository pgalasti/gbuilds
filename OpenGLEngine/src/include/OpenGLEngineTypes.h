#ifndef OPENGL_ENGINE_TYPES_H
#define OPENGL_ENGINE_TYPES_H

#include "StdHeader.h"
#include "PipelineDefs.h"
#include "ShaderTypes.h"

namespace GApi {
	namespace Graphics {
		namespace OpenGL {

			using GApi::Graphics::ShaderTypesEnum;
			
			GLenum GetOpenGLEnum(ShaderTypesEnum enumType)
			{
				switch (enumType)
				{

				case VertexShader:
					return GL_VERTEX_SHADER;
				case FragmentShader:
					return GL_FRAGMENT_SHADER;
				default:
					return 0;

				}
			}
		}
	}
}

#endif