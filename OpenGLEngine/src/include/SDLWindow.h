#ifndef SDL_WINDOW_H
#define SDL_WINDOW_H

#include "StdHeader.h"

#include "IGameWindow.h"
#include "IGameEngine.h"
#include "GameWindowImpl.h"

#include <SDL.h>
#include <GL/glew.h>
#include <string>
#include "GTimer.h"

using GApi::UI::WindowOptions;
using GApi::UI::IGameWindow;
using GApi::UI::GameWindowImpl;
using GApi::Util::Timer;

using GApi::GameEngine::IGameEngine;

namespace GApi {
	namespace OpenGL {
		namespace Window {

			using GApi::Graphics::IGraphicsEngine;
			using GApi::Graphics::GraphicsEngineOptions;

			class DllExport SDLGameWindow : public GameWindowImpl
			{
			public:
				SDLGameWindow();
				virtual ~SDLGameWindow();

				virtual BOOL Initialize(const WindowOptions& options) override;
				virtual BOOL Cleanup() override;
				virtual BOOL Start() override;
				virtual BOOL Run(const float32 deltaTime) override;
				virtual inline void SetWindowTitle(const char8* pszName) override;
				virtual inline BOOL SetGameEngine(IGameEngine* pEngine) override;
				virtual BOOL GetWindowHandle(void* ptr) override;
			
			private:

				SDL_Window* m_pWindow;
				SDL_GLContext m_glContext;

			};

		}
	}
}
#endif