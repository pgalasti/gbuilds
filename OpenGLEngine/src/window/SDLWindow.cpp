#include "SDLWindow.h"
#include <iostream>

using GApi::OpenGL::Window::SDLGameWindow;
using GApi::Graphics::IGraphicsEngine;
using GApi::Graphics::GraphicsEngineOptions;

SDLGameWindow::SDLGameWindow()
{
	m_pWindow = nullptr;
	m_Running = false;
}

SDLGameWindow::~SDLGameWindow()
{

}

BOOL SDLGameWindow::Initialize(const WindowOptions& options)
{ 
	if (!GameWindowImpl::Initialize(options))
		return FALSE;

	SDL_Init(SDL_INIT_EVENTS | SDL_INIT_VIDEO);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	if (strlen(options.WindowTitle))
		this->SetWindowTitle(options.WindowTitle);

	m_pWindow = SDL_CreateWindow(m_Title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, (int)options.Width, (int)options.Height, SDL_WINDOW_OPENGL);
	m_glContext = SDL_GL_CreateContext(m_pWindow);

	GLenum status = glewInit();
	if (status != GLEW_OK)
	{
		std::cerr << "Glew failed to initialize!" << std::endl;
		return FALSE;
	}

	SDL_GL_SetSwapInterval(0);

	return TRUE; 
}

BOOL SDLGameWindow::Cleanup()
{
	if (!GameWindowImpl::Cleanup())
		return FALSE;

	SDL_GL_DeleteContext(m_glContext);

	SDL_DestroyWindow(m_pWindow);
	m_pWindow = nullptr;

	SDL_Quit();

	return TRUE;
}

BOOL SDLGameWindow::Start()
{
	if (!GameWindowImpl::Start())
		return FALSE;

	

	return TRUE;
}

BOOL SDLGameWindow::Run(const float32 deltaTime)
{
	if (!GameWindowImpl::Run(deltaTime))
		return FALSE;

	SetWindowTitle(m_Title.c_str());
	
	SDL_GL_SwapWindow(m_pWindow);

	SDL_Event sdlEvent;
	while (SDL_PollEvent(&sdlEvent))
	{
		if (sdlEvent.type == SDL_QUIT)
			m_Running = false;
	}

	return TRUE;
}

void SDLGameWindow::SetWindowTitle(const char8* pszName)
{
	this->m_Title = pszName;
	std::string fullTitle = m_Title + std::string(" - ") + std::to_string((int)m_LastFPS) + " FPS";
	SDL_SetWindowTitle(m_pWindow, fullTitle.c_str());
}

BOOL SDLGameWindow::SetGameEngine(IGameEngine* pEngine)
{
	m_pGameEngine = pEngine;
	return TRUE;
};

BOOL SDLGameWindow::GetWindowHandle(void* ptr)
{
	return TRUE;
}

