#include "OpenGLEngine.h"
#include <GL/glew.h>
#include "OpenGLConversions.h"
#include <memory>

#include "OpenGLUtils.h"
#include "GStringUtils.h"
#include "GMesh.h"

using namespace GApi::Graphics::OpenGL;

using GApi::Graphics::OpenGL::OpenGLEngine;
using GApi::Graphics::ShaderTypesEnum;
using GApi::Graphics::RGBAColor;

OpenGLEngine::OpenGLEngine()
{
	m_bOptionsAreSet = false;
}

BOOL OpenGLEngine::SetOptions(const GraphicsEngineOptions& options)
{
	m_Options = options;
	m_bOptionsAreSet = true;
	return TRUE;
}

BOOL OpenGLEngine::SetWindowHandle(void* ptr)
{
	return TRUE;
}

BOOL OpenGLEngine::Startup()
{
	if (!m_bOptionsAreSet)
	{
		// Log
		return FALSE;
	}

	std::string executableDirectory = FileResolver::GetExecutableDirectory().c_str();
	m_ShaderPathResolver.SetRootPath(executableDirectory.c_str());
	m_ResourcePathResolver.SetRootPath(std::string(executableDirectory).append("Resources").c_str());

	m_pMeshIDGenerator = new IDGenerator<MeshID>(1, 100);
	m_pTextureIDGenerator = new IDGenerator<TextureID>(1, 100);

	// Create shaders
	if (!CreateShaderProgram("TestShader"))
		return FALSE;
	if (!AttachShaderToProgram("test_vertex.glsl", "TestShader", VertexShader))
		return FALSE;
	if (!AttachShaderToProgram("test_fragment.glsl", "TestShader", FragmentShader))
		return FALSE;

	GLuint program = m_ShaderProgramMap["TestShader"];
	glLinkProgram(program);

	std::string errorMessage;
	if (!Utility::CheckProgramError(program, errorMessage))
	{
		// Log
		return FALSE;
	}

	const float32 width = m_Options.width;
	const float32 height = m_Options.height;
	m_pCamera = CameraPtr(new Camera(
		//Vector3D(0.0f, 0.0f, 1.0f),   // Position
		Vector3D(0.0f, 0.0f, 1.0f),   // Position
		Vector3D(0.0f, 0.0f, 0.00f), // Focus
		Vector3D(0.0f, 1.0f, 0.0f), // Up Vector
		(float32)m_Options.width / (float32)m_Options.height, // Aspect Ratio
		0.10f,	// Near Plane
		1000.00f // Far Plane
		//(float)GApi::GMath::PI_F / 2 // Field of view
		));


	return TRUE;
}

BOOL OpenGLEngine::Shutdown()
{
	// Clear out any shader programs
	for (auto iter : m_ShaderProgramMap)
	{
		std::string programName = iter.first;
		if (!this->DeleteShaderProgram(programName.c_str()))
			return FALSE;
	}
	m_ShaderProgramMap.clear();

	// Deregister any meshes remaining
	std::vector<MeshID> meshIDList;
	meshIDList.reserve(m_MeshMap.size());
	for (auto iter : m_MeshMap)
		meshIDList.push_back(iter.first);
	for (auto iter : meshIDList)
		this->DeregisterMesh(iter);

	// Deregister any textures remaining.
	std::vector<TextureID> textureIDList;
	for (auto iter : m_TextureMap)
		textureIDList.push_back(iter.first);
	for (auto iter : textureIDList)
		this->DeregisterTexture(iter);

	m_pCamera.reset();
	SAFE_DELETE(m_pMeshIDGenerator);
	SAFE_DELETE(m_pTextureIDGenerator);

	m_bOptionsAreSet = false;

	return TRUE;
}

BOOL OpenGLEngine::Render()
{
	for (auto iter : m_MeshMap)
	{
		const MeshID meshID = iter.first;
		const OpenGLMeshPtr pMesh = iter.second;

		const GLuint program = m_MeshToProgramMap[meshID];
		glUseProgram(program);

		const GLuint vertexArrayObject = pMesh->GetVertexArrayBuffer();
		glBindVertexArray(vertexArrayObject);
		
		// If there's no transformation registered for the mesh, just continue;
		auto meshIter = m_MeshTransformationMap.find(meshID);
		if (meshIter == m_MeshTransformationMap.end())
			continue;

		// Apply the transformation
		Transform transformation = meshIter->second;
		GLuint uniform = glGetUniformLocation(program, "MVPMatrix");
		Matrix4x4F transformationMatrix = transformation.GetTransformationMatrix();
		Matrix4x4F projectionViewMatrix = m_pCamera->GetProjectionViewMatrix(Camera::Major::Column);
		Matrix4x4F modelViewProjectionMatrix = projectionViewMatrix*transformationMatrix;

		glUniformMatrix4fv(uniform, 1, GL_TRUE, &modelViewProjectionMatrix.valuesStruct.Values.r1c1);

		glBindTexture(GL_TEXTURE_2D, 0);

		// Apply the texture
		auto meshTextureIter = m_MeshToTextureMap.find(meshID);
		if (meshTextureIter != m_MeshToTextureMap.end())
		{
			TextureID textureID = meshTextureIter->second;
			auto textureIter = m_TextureMap.find(textureID);
			if (textureIter == m_TextureMap.end())
			{
				// Log
				return FALSE;
			}

			ITexturePtr pTexture = textureIter->second;
			if (!pTexture->ApplyTexture())
			{
				// Log
				return FALSE;
			}
		}

		const uint32 vertexCount = pMesh->GetVertexCount();
		glDrawArrays(GL_TRIANGLES, 0, vertexCount);

		glBindVertexArray(0);
	}

	// Remove frame transformations.
	m_MeshTransformationMap.clear();

	return TRUE;
}

BOOL OpenGLEngine::ClearScreen(const RGBAColor& clearColor)
{
	GLclampf color[4];
	ToGLColor(clearColor, &color[0]);
	glClearColor(color[0], color[1], color[2], color[3]);

	glClear(GL_COLOR_BUFFER_BIT);

	return TRUE;
}

BOOL OpenGLEngine::RegisterMesh(Mesh& mesh, MeshID& meshID)
{
	const MeshID newMeshID = m_pMeshIDGenerator->GetNextId();
	m_MeshMap[newMeshID] = OpenGLMeshPtr(new OpenGLMesh(mesh));
	meshID = newMeshID;
	
	return TRUE;
}

BOOL OpenGLEngine::DeregisterMesh(const MeshID meshID)
{
	OpenGLMeshPtr meshPtr = m_MeshMap[meshID];
	m_MeshMap.erase(meshID);

	m_pMeshIDGenerator->Release(meshID);
	
	auto meshToTexterIter = m_MeshToTextureMap.find(meshID);
	if (meshToTexterIter != m_MeshToTextureMap.end())
		m_MeshToTextureMap.erase(meshID);

	auto meshToProgramIter = m_MeshToProgramMap.find(meshID);
	if (meshToProgramIter != m_MeshToProgramMap.end())
		m_MeshToProgramMap.erase(meshID);

	return TRUE;
}

BOOL OpenGLEngine::CopyMesh(const MeshID meshID, MeshID& newMesh)
{
	const MeshID newMeshID = m_pMeshIDGenerator->GetNextId();
	newMesh = newMeshID;

	OpenGLMeshPtr meshPtr = m_MeshMap[meshID];
	m_MeshMap[newMeshID] = meshPtr;

	return TRUE;
}

BOOL OpenGLEngine::BindShaderToMesh(const char8* pszProgramName, const MeshID meshID)
{
	const std::string shaderName = pszProgramName;
	if (m_ShaderProgramMap.find(shaderName) == m_ShaderProgramMap.end())
	{
		// Log
		return FALSE;
	}

	const GLuint program = m_ShaderProgramMap[shaderName];
	m_MeshToProgramMap[meshID] = program;

	return TRUE;
}

BOOL OpenGLEngine::TransformMesh(const MeshID meshID, const Transform& transformation)
{
	if (m_MeshTransformationMap.find(meshID) != m_MeshTransformationMap.end())
	{
		// Log
		return FALSE;
	}

	m_MeshTransformationMap[meshID] = transformation;
	return TRUE;
}

BOOL OpenGLEngine::RegisterTexture(std::string resourceName, TextureID& textureID)
{
	textureID = 0;

	std::string fullResourcePath;
	m_ResourcePathResolver.GetFilePath(resourceName.c_str(), fullResourcePath);

	GLuint glTextureID;
	glGenTextures(1, &glTextureID);

	ITexturePtr pTexture = nullptr;
	
	if (GApi::Util::StringEndsWith(resourceName, std::string(".dds"), true))
		pTexture = ITexturePtr(new DDSTexture(glTextureID));
	else if (GApi::Util::StringEndsWith(resourceName, std::string(".jpg"), true))
		pTexture = ITexturePtr(new JpegTexture(glTextureID));

	if (!pTexture->LoadTexture(fullResourcePath.c_str()))
	{
		// Log
		return FALSE;
	}

	textureID = m_pTextureIDGenerator->GetNextId();
	m_TextureMap[textureID] = pTexture;

	return TRUE;
}

BOOL OpenGLEngine::DeregisterTexture(const TextureID textureID)
{
	auto textureIter = m_TextureMap.find(textureID);
	if (textureIter == m_TextureMap.end())
	{
		// Log
		return FALSE;
	}
	ITexturePtr ptr = textureIter->second;
	ptr->FreeTexture();
	ptr = nullptr;

	m_TextureMap.erase(textureID);
	m_pTextureIDGenerator->Release(textureID);

	return TRUE;
}

BOOL OpenGLEngine::BindTextureToMesh(const TextureID textureID, const MeshID meshID)
{
	if (m_MeshToTextureMap.find(meshID) != m_MeshToTextureMap.end())
	{
		// Log
		return FALSE;
	}

	if (m_MeshMap.find(meshID) == m_MeshMap.end())
	{
		// Log
		return FALSE;
	}

	if (m_TextureMap.find(textureID) == m_TextureMap.end())
	{
		// Log
		return FALSE;
	}

	m_MeshToTextureMap[meshID] = textureID;
	return TRUE;
}

std::string OpenGLEngine::GetEngineDescription() const
{
	return "OpenGL Engine"; // Version, etc..
}

BOOL OpenGLEngine::CreateShaderProgram(const char8* pszProgramName)
{
	const std::string KEY = pszProgramName;
	ShaderProgramMap::iterator iter = m_ShaderProgramMap.find(KEY);
	if (iter != m_ShaderProgramMap.end())
	{
		// Log
		return FALSE;
	}

	m_ShaderProgramMap[KEY] = glCreateProgram();
	return TRUE;
}

BOOL OpenGLEngine::DeleteShaderProgram(const char8* pszProgramName)
{
	const std::string KEY = pszProgramName;
	ShaderProgramMap::iterator iter = m_ShaderProgramMap.find(KEY);
	if (iter == m_ShaderProgramMap.end())
	{
		// Log
		return FALSE;
	}

	const GLuint program = iter->second;

	// Detach any shaders from program
	for (auto programToShaderIter : m_ProgramToShaderMap)
	{
		const GLuint shader = programToShaderIter.second;
		glDetachShader(program, shader);
		glDeleteShader(shader);
	}
	m_ProgramToShaderMap.erase(program);

	// Delete program
	glDeleteProgram(program);

	return TRUE;
}

BOOL OpenGLEngine::AttachShaderToProgram(const char8* pszShaderProgramName, const char8* pszProgramName, ShaderTypesEnum shaderType)
{
	const std::string KEY = pszProgramName;
	ShaderProgramMap::iterator iter = m_ShaderProgramMap.find(KEY);
	if (iter == m_ShaderProgramMap.end())
	{
		// Log
		return FALSE;
	}
	GLuint program = iter->second;
	
	// Get shader code
	std::string shaderFilePath;
	m_ShaderPathResolver.GetFilePath(pszShaderProgramName, shaderFilePath);
	char szShaderCode[10240];
	GApi::IO::LoadFile(shaderFilePath.c_str(), szShaderCode, 10240);
	const GLchar* pShaderCode = szShaderCode;

	// Compile shader
	GLenum shaderEnum = GetOpenGLEnum(shaderType);
	GLuint shaderName = glCreateShader(shaderEnum);
	glShaderSource(shaderName, 1, &pShaderCode, NULL);
	glCompileShader(shaderName);

	// Attach to shader program
	glAttachShader(program, shaderName);

	std::string errorMessage;
	if (!Utility::CheckShaderError(shaderName, errorMessage))
	{
		// Log
		return FALSE;
	}

	m_ProgramToShaderMap.insert(std::make_pair(program, shaderName));


	return TRUE;
}

std::shared_ptr<Camera> OpenGLEngine::GetCamera()
{
	return m_pCamera;
}