#include "OpenGLUtils.h"

namespace GApi {
	namespace Graphics {
		namespace OpenGL {
			namespace Utility {

				
				BOOL CheckShaderError(GLuint shader, std::string& errorMessage)
				{
					errorMessage.clear();
					GLint successCode = GL_TRUE;

					glGetShaderiv(shader, GL_COMPILE_STATUS, &successCode);

					if (successCode == GL_FALSE)
					{
						char szError[1024];
						glGetShaderInfoLog(shader, sizeof(szError), NULL, szError);

						errorMessage = szError;
						return FALSE;
					}

					return TRUE;
				}

				BOOL CheckProgramError(GLuint program, std::string& errorMessage)
				{
					errorMessage.clear();
					GLint successCode = GL_TRUE;

					glGetShaderiv(program, GL_LINK_STATUS, &successCode);

					if (successCode == GL_FALSE)
					{
						char szError[1024];
						glGetShaderInfoLog(program, sizeof(szError), NULL, szError);

						errorMessage = szError;
						return FALSE;
					}

					glValidateProgram(program);
					glGetShaderiv(program, GL_VALIDATE_STATUS, &successCode);

					if (successCode == GL_FALSE)
					{
						char szError[1024];
						glGetShaderInfoLog(program, sizeof(szError), NULL, szError);

						errorMessage = szError;
						return FALSE;
					}

					return TRUE;
				}

			}
		}
	}
}