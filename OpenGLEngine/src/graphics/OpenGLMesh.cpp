#include "OpenGLMesh.h"

using namespace GApi::Graphics;
using namespace GApi::Graphics::OpenGL;

OpenGLMesh::OpenGLMesh(Mesh& mesh)
	: m_VertexArrayObject(0), m_VertexArrayBuffer(0), m_VertexCount(0)
{

	glGenVertexArrays(1, &m_VertexArrayObject);
	glBindVertexArray(m_VertexArrayObject);

	glGenBuffers(1, &m_VertexArrayBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_VertexArrayBuffer);
	
	Vertex* pVerticies = nullptr;
	mesh.GetVerticiesPtr(&pVerticies);
	glBufferData(GL_ARRAY_BUFFER, mesh.GetNumberVerticies() * sizeof(pVerticies[0]), pVerticies, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);

	m_VertexCount = mesh.GetNumberVerticies();
}

OpenGLMesh::~OpenGLMesh()
{
	glDeleteVertexArrays(1, &m_VertexArrayObject);
}