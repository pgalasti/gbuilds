#include "OpenGLTexture.h"

#include "GStringUtils.h"

#include "stb_image.h"

using namespace GApi::Graphics;
using namespace GApi::Graphics::OpenGL;

DDSTexture::DDSTexture(GLuint textureObjectID)
	: OpenGLTextureImpl(textureObjectID)
{

}

DDSTexture::~DDSTexture()
{

}

BOOL DDSTexture::LoadTexture(const char8* pszFileName)
{
	if (GApi::Util::StringEndsWith(pszFileName, ".dds", true))
		return LoadDDS(pszFileName);
	
	return FALSE;
}

BOOL DDSTexture::ApplyTexture()
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_TextureObjectID);

	return TRUE;
}

void DDSTexture::FreeTexture()
{
	m_DDSImage.clear();
}

BOOL DDSTexture::LoadDDS(const char8* pszFileName)
{
	m_DDSImage.clear();
	m_DDSImage.load(pszFileName);

	if (!m_DDSImage.is_valid())
	{
		// Log
		return FALSE;
	}

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_TextureObjectID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	m_DDSImage.upload_texture2D();

	return TRUE;
}

JpegTexture::JpegTexture(GLuint textureObjectID)
	: OpenGLTextureImpl(textureObjectID)
{
	
}

JpegTexture::~JpegTexture()
{

}

BOOL JpegTexture::LoadTexture(const char8* pszFileName)
{
	if (GApi::Util::StringEndsWith(pszFileName, ".jpg", true))
		return LoadJpeg(pszFileName);

	return FALSE;
}

BOOL JpegTexture::ApplyTexture()
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_TextureObjectID);
	return TRUE;
}

void JpegTexture::FreeTexture()
{
	glDeleteTextures(1, &m_TextureObjectID);
}

BOOL JpegTexture::LoadJpeg(const char8* pszFileName)
{
	int32 width, height, numComponents;
	ubyte8* pData = stbi_load(pszFileName, &width, &height, &numComponents, 4);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_TextureObjectID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pData);

	stbi_image_free(pData);

	return TRUE;
}