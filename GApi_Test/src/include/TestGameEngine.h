#ifndef TEST_GAME_ENGINE_H
#define TEST_GAME_ENGINE_H

#include "StdHeader.h"
#include "IGameEngine.h"
#include "IGraphicsEngine.h"
#include "GColor.h"
#include "GMesh.h"
#include "GMatrix.h"
#include "GTransform.h"

using GApi::GameEngine::IGameEngine;
using GApi::Graphics::IGraphicsEngine;
using GApi::Graphics::GraphicsEngineOptions;
using GApi::Graphics::Transform;
using GApi::GMath::Matrix4x4F;

class TestGameEngine : implements IGameEngine
{
	virtual BOOL Initialize() { return TRUE; }
		
		
	virtual BOOL SetGraphicsEngine(IGraphicsEngine* pGraphicsEngine, const GraphicsEngineOptions& options) 
	{ 
		m_pGraphicsEngine = pGraphicsEngine;
		return TRUE; 
	}

	virtual BOOL Start() 
	{ 
		if (!m_pGraphicsEngine->Startup())
			return FALSE;

		GApi::Graphics::Vertex vertices[8];
		vertices[0].position.x = -1.0f;
		vertices[0].position.y = -1.0f;
		vertices[0].position.z = -1.0f;

		vertices[1].position.x = -1.0f;
		vertices[1].position.y = +1.0f;
		vertices[1].position.z = -1.0f;

		vertices[2].position.x = +1.0f;
		vertices[2].position.y = +1.0f;
		vertices[2].position.z = -1.0f;

		vertices[3].position.x = +1.0f;
		vertices[3].position.y = -1.0f;
		vertices[3].position.z = -1.0f;

		vertices[4].position.x = -1.0f;
		vertices[4].position.y = -1.0f;
		vertices[4].position.z = +1.0f;

		vertices[5].position.x = -1.0f;
		vertices[5].position.y = +1.0f;
		vertices[5].position.z = +1.0f;

		vertices[6].position.x = +1.0f;
		vertices[6].position.y = +1.0f;
		vertices[6].position.z = +1.0f;

		vertices[7].position.x = +1.0f;
		vertices[7].position.y = -1.0f;
		vertices[7].position.z = +1.0f;

		ushort16 test[36] =
		{
			// front face
			0, 1, 2,
			0, 2, 3,

			// back face
			4, 6, 5,
			4, 7, 6,

			// left face
			4, 5, 1,
			4, 1, 0,

			// right face
			3, 2, 6,
			3, 6, 7,

			// top face
			1, 5, 6,
			1, 6, 2,

			// bottom face
			4, 0, 3,
			4, 3, 7
		};

		GApi::Graphics::Mesh testMesh;
		testMesh.SetVerticies(vertices, 8);
		testMesh.SetIndicies(test, 36);
		GApi::Graphics::MeshID id;
		if (!m_pGraphicsEngine->RegisterMesh(testMesh, id))
			return FALSE;
		if (!m_pGraphicsEngine->BindShaderToMesh("TestShader", id))
			return FALSE;

		//GApi::Graphics::MeshID copyId;

		//if (!m_pGraphicsEngine->CopyMesh(id, copyId))
		//	return FALSE;
		//if (!m_pGraphicsEngine->BindShaderToMesh("TestShader", copyId))
		//	return FALSE;

		//GApi::Graphics::TextureID textureID;
		////if (!m_pGraphicsEngine->RegisterTexture("seafloor.dds", textureID))
		////	return FALSE;
		//if (!m_pGraphicsEngine->RegisterTexture("detroit.jpg", textureID))
		//	return FALSE;
		//if (!m_pGraphicsEngine->BindTextureToMesh(textureID, copyId))
		//	return FALSE;
		//if (!m_pGraphicsEngine->BindTextureToMesh(textureID, id))
		//	return FALSE;

		return TRUE; 
	}

	virtual BOOL Stop() { return TRUE; }
	virtual BOOL Pause() { return TRUE; }

	virtual BOOL Cleanup() 
	{ 
		if(!m_pGraphicsEngine->Shutdown())
			return FALSE;

		return TRUE; 
	}

	virtual BOOL UpdateState(const float32 deltaTime)
	{
		if (!m_pGraphicsEngine->ClearScreen(GApi::Graphics::RGBAColor::DefaultBlue()))
			return FALSE;
		
		// Script logic will go here. For now we're using hard coded crap

		//Transform tranform;
		//
		//static float32 x = 0;
		//static float32 y = 0;
		//static float32 totalTime = 0.0;
		//x += deltaTime * .25f;
		//y = cos(totalTime);
		//totalTime += deltaTime;
		//tranform.Translate(-y, y, 0);
		//tranform.Scale(x, x, 0);
		//m_pGraphicsEngine->TransformMesh(1, tranform);

		//Transform tranform2;
		//tranform2.Translate(.5, 0, 0);
		//tranform2.Rotate(x, Transform::Axis::Z);
		//m_pGraphicsEngine->TransformMesh(2, tranform2);
		//
		//auto camera = m_pGraphicsEngine->GetCamera();
		//camera->SetPosition(GApi::GMath::Vector3D(0.0f, 0.0f, 1.0));
		
		if (!m_pGraphicsEngine->Render())
			return FALSE;

		return TRUE;
	}
	
	inline virtual IGraphicsEngine* GetGraphicsEngine() const
	{
		return m_pGraphicsEngine;
	}

protected:
	IGraphicsEngine* m_pGraphicsEngine;
};

#endif