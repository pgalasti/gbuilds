#include "StdHeader.h"
#include "GVector.h"
#include "GPoint.h"
#include <iostream>
#include "GMatrix.h"
#include "GMath.h"

#include "GameWindowImpl.h"
#include "Win32Window.h"
#include "SDLWindow.h"
#include "IGameEngine.h"
#include "TestGameEngine.h"
#include "OpenGLEngine.h"

#include "OpenGLConversions.h"

#include "GMesh.h"
#include "GVector.h"
#include "GTexture.h"
#include "OpenGLTexture.h"

#include <DirectXMath.h>
#include "DirectX12Engine.h"
#include "GStringUtils.h"

using namespace GApi::DirectX::Window;

#undef main

#ifdef _WIN32
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
#else
int main(int argc, char** argv)
#endif

{

	//GApi::UI::GameWindowImpl* pGameWindow = new GApi::DirectX::Window::Win32GameWindow(hInstance);
	//WindowOptions windowOptions;
	//windowOptions.Height = 600;
	//windowOptions.Width = 1280;
	//
	//GApi::GameEngine::IGameEngine* pGameEngine = new TestGameEngine();

	//IGraphicsEngine* pGraphicsEngine = new GApi::DirectX::Engine::DirectX12Engine();
	//GraphicsEngineOptions graphicEngineOptions;
	//graphicEngineOptions.height = windowOptions.Height;
	//graphicEngineOptions.width = windowOptions.Width;
	//graphicEngineOptions.FullScreen = false;

	//if (!pGameEngine->SetGraphicsEngine(pGraphicsEngine, graphicEngineOptions))
	//	return -1;

	//if (!pGameWindow->SetGameEngine(pGameEngine))
	//	return -1;

	//if (!pGameWindow->Initialize(windowOptions))
	//	return -1;

	//if (!pGameWindow->Start())
	//	return -1;

	//if (!pGameWindow->Cleanup())
	//	return -1;

	//SAFE_DELETE(pGameWindow);

// ======================================================================================================
	

	GApi::GameEngine::IGameEngine* pGameEngine = new TestGameEngine();

	
	//GApi::UI::GameWindowImpl* pGameWindow = new GApi::OpenGL::Window::SDLGameWindow();
	GApi::UI::GameWindowImpl* pGameWindow = new Win32GameWindow(hInstance);
	GApi::UI::WindowOptions options;
	ZeroMemory(&options, sizeof(GApi::UI::WindowOptions));
	options.Width = 800;
	options.Height = 600;
	strcpy(options.WindowTitle, "Hello Framework");

	
	GraphicsEngineOptions graphicsOptions;
	graphicsOptions.height = (float32)options.Height;
	graphicsOptions.width = (float32)options.Width;
	graphicsOptions.FullScreen = false;
	graphicsOptions.swapChainCount = 2;
	
	//GApi::Graphics::IGraphicsEngine* pGraphicsEngine = new GApi::Graphics::OpenGL::OpenGLEngine();
	GApi::Graphics::IGraphicsEngine* pGraphicsEngine = new GApi::DirectX::Engine::DirectX12Engine();
	if (!pGraphicsEngine->SetOptions(graphicsOptions))
		return -1;

	if (!pGameEngine->SetGraphicsEngine(pGraphicsEngine, graphicsOptions))
		return -1;

	if (!pGameWindow->SetGameEngine(pGameEngine))
		return -1;

	if (!pGameWindow->Initialize(options))
		return -1;

#ifdef _WIN32
	HWND hWnd;
	pGameWindow->GetWindowHandle(&hWnd);
	pGraphicsEngine->SetWindowHandle(&hWnd);
#endif

	if (!pGameWindow->Start())
		return -1;

	if (!pGameWindow->Cleanup())
		return -1;

	SAFE_DELETE(pGameWindow);

	return 0;
}