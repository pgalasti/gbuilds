#ifdef _WIN32
#include <Windows.h>
#elif LINUX
#include <chrono>
#endif

#include "GTimer.h"

using namespace GApi::Util;

Timer::Timer()
	: mSecondsPerCount(1.0), mDeltaTime(-1.0), mBaseTime(0), mPausedTime(0), mPrevTime(0), mCurrTime(0), mStopped(false)
{
#ifdef _WIN32

	int64 countsPerSec;
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
	mSecondsPerCount = 1.0 / (double)countsPerSec;

#elif LINUX
	mSecondsPerCount = 1.0;
#endif

}

float32 Timer::TotalTime() const
{
	// If we are stopped, do not count the time that has passed since we stopped.
	// Moreover, if we previously already had a pause, the distance 
	// mStopTime - mBaseTime includes paused time, which we do not want to count.
	// To correct this, we can subtract the paused time from mStopTime:  
	//
	//                     |<--paused time-->|
	// ----*---------------*-----------------*------------*------------*------> time
	//  mBaseTime       mStopTime        startTime     mStopTime    mCurrTime

	if (mStopped)
	{
		return (float32)(((mStopTime - mPausedTime) - mBaseTime)*mSecondsPerCount);
	}

	// The distance mCurrTime - mBaseTime includes paused time,
	// which we do not want to count.  To correct this, we can subtract 
	// the paused time from mCurrTime:  
	//
	//  (mCurrTime - mPausedTime) - mBaseTime 
	//
	//                     |<--paused time-->|
	// ----*---------------*-----------------*------------*------> time
	//  mBaseTime       mStopTime        startTime     mCurrTime

	else
	{
		return (float32)(((mCurrTime - mPausedTime) - mBaseTime)*mSecondsPerCount);
	}
}

float32 Timer::DeltaTime() const
{
#ifdef _WIN32
	return (float32)mDeltaTime;
#elif LINUX
	return (float32)mDeltaTime*.001;
#endif
}

void Timer::Reset()
{
	int64 currTime;
#ifdef _WIN32
	QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
#elif LINUX
	auto now = std::chrono::system_clock::now();
	auto duration = now.time_since_epoch();
	currTime = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
#endif

	mBaseTime = currTime;
	mPrevTime = currTime;
	mStopTime = 0;
	mStopped = false;
}

void Timer::Start()
{
	int64 startTime;

#ifdef _WIN32
	QueryPerformanceCounter((LARGE_INTEGER*)&startTime);
#elif LINUX
	auto now = std::chrono::system_clock::now();
	auto duration = now.time_since_epoch();
	startTime = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
#endif

	// Accumulate the time elapsed between stop and start pairs.
	//
	//                     |<-------d------->|
	// ----*---------------*-----------------*------------> time
	//  mBaseTime       mStopTime        startTime     

	if (mStopped)
	{
		mPausedTime += (startTime - mStopTime);

		mPrevTime = startTime;
		mStopTime = 0;
		mStopped = false;
	}
}

void Timer::Stop()
{
	if (!mStopped)
	{
		int64 currTime;

#ifdef _WIN32
		QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
#elif LINUX
		auto now = std::chrono::system_clock::now();
		auto duration = now.time_since_epoch();
		currTime = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
#endif
		mStopTime = currTime;
		mStopped = true;
	}
}

void Timer::Tick()
{
	if (mStopped)
	{
		mDeltaTime = 0.0;
		return;
	}

	int64 currTime;

#ifdef _WIN32
	QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
#elif LINUX
	auto now = std::chrono::system_clock::now();
	auto duration = now.time_since_epoch();
	currTime = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
#endif

	mCurrTime = currTime;

	// Time difference between this frame and the previous.
	mDeltaTime = (mCurrTime - mPrevTime)*mSecondsPerCount;

	// Prepare for next frame.
	mPrevTime = mCurrTime;

	// Force nonnegative.  The DXSDK's CDXUTTimer mentions that if the 
	// processor goes into a power save mode or we get shuffled to another
	// processor, then mDeltaTime can be negative.
	if (mDeltaTime < 0.0)
	{
		mDeltaTime = 0.0;
	}
}