#include "GStringUtils.h"

#include <cctype>

#include <locale>
#include <codecvt>
#include <string>

namespace GApi {
	namespace Util {

		const bool StringEndsWith(char8* const pszString, char8* const pszEndsWith, bool ignoreCase/* = false*/)
		{
			const uint32 stringSize = strlen(pszString);
			const uint32 checkStringSize = strlen(pszEndsWith);
			if (stringSize < checkStringSize)
				return false;

			char8* pString = pszString;
			char8* pCheckString = pszEndsWith;

			pString += stringSize - checkStringSize;

			for (uint32 i = 0; i < checkStringSize; i++)
			{
				if (ignoreCase)
				{
					if (tolower(pString[i]) != tolower(pCheckString[i]))
						return false;
				}
				else
				{
					if (pString[i] != pCheckString[i])
						return false;
				}

			}

			return true;
		}

		const bool StringEndsWith(const std::string stringToCheck, const std::string endsWith, bool ignoreCase /*= false*/)
		{
			char8* pszString = (char8*)stringToCheck.c_str();
			char8* pszEndsWith = (char8*)endsWith.c_str();

			return StringEndsWith(pszString, pszEndsWith, ignoreCase);
		}

		const std::wstring Convert(const std::string str)
		{
			std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
			return converter.from_bytes(str);
		}

	}
}