#ifdef _WIN32
#include <Shlwapi.h>
#elif LINUX
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <libgen.h>
#endif

#include "GFileApi.h"

#include "StdHeader.h"
#include <string>

class GApi::IO::Impl
{
public:
	Impl() {}
	std::string DirectoryPath;
};

BOOL GApi::IO::LoadFile(const char8* pszFilePath, char8* pszFileOutput, uint32 maxLength)
{
	FILE* pFile = fopen(pszFilePath, "r+");
	if (IsNull(pFile))
		return FALSE;

	memset(pszFileOutput, 0, maxLength);
	char szLine[1024];
	while (fgets(szLine, 1024, pFile))
		strcat(pszFileOutput, szLine);

	return TRUE;
}

using GApi::IO::FileResolver;

FileResolver::FileResolver() : m_pImpl(new GApi::IO::Impl())
{

}
FileResolver::~FileResolver() 
{
	SAFE_DELETE(m_pImpl);
}

BOOL FileResolver::SetRootPath(const char8* pszDirectoryPath)
{
#ifdef _WIN32
	if (!PathFileExists(pszDirectoryPath))
	{
		// Log
		return FALSE;
	}
#elif LINUX
	if(!S_ISDIR(st.st_mode))
	{
		// Log
		return FALSE;
	}
#endif

	m_pImpl->DirectoryPath = pszDirectoryPath;

	return TRUE;
}

BOOL FileResolver::GetFilePath(const char8* pszFileName, std::string& outputPath)
{
	if (m_pImpl->DirectoryPath.empty())
	{
		// Log
		return FALSE;
	}

	outputPath.clear();
	outputPath = m_pImpl->DirectoryPath;
	if (outputPath.at(outputPath.length() - 1) != FILE_SEPERATOR_CHAR)
		outputPath += FILE_SEPERATOR_CHAR;

	outputPath.append(pszFileName);

	return TRUE;
}

/*static */std::string FileResolver::GetExecutableDirectory()
{
	std::string directory;
#ifdef _WIN32
	char szPath[_MAX_PATH];
	GetModuleFileName(NULL, szPath, _MAX_PATH);

	char szDrive[_MAX_PATH];
	char szDirectory[_MAX_PATH];
	_splitpath(szPath, szDrive, szDirectory, nullptr, nullptr);
	directory.append(szDrive).append(szDirectory);

#elif LINUX
	char szPath[1024];
	readlink("/proc/self/exe", szPath, 1024);
	directory = dirname(szPath);
#endif

	return directory;
}