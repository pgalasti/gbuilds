#ifndef G_TEXTURE_TYPES
#define G_TEXTURE_TYPES

#include "StdHeader.h"

namespace GApi {
	namespace Graphics {

		struct DllExport DDSHeader {
			uint32_t dwSize;
			uint32_t dwFlags;
			uint32_t dwHeight;
			uint32_t dwWidth;
			uint32_t dwPitchOrLinearSize;
			uint32_t dwDepth;
			uint32_t dwMipMapCount;
			uint32_t dwReserved1[11];
			DDS_PIXELFORMAT ddspf;
			uint32_t dwCaps1;
			uint32_t dwCaps2;
			uint32_t dwReserved2[3];
		};

		struct DllExport DDSImageData
		{
			uint32 Width;
			uint32 Height;
			uint32 Components;
			uint32 Format;
			int32 MipMaps;
			ubyte8* pData;
		};
	}
}

#endif