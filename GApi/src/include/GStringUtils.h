#ifndef G_STRING_UTILS
#define G_STRING_UTILS

#include "StdHeader.h"

#include<string>

namespace GApi {
	namespace Util {

#define STRING_COMPARE_EQUAL 0

		DllExport const bool StringEndsWith(char8* const pszString, char8* const pszEndsWith, bool ignoreCase = false);
		DllExport const bool StringEndsWith(const std::string stringToCheck, const std::string endsWith, bool ignoreCase = false);
		DllExport const std::wstring Convert(const std::string str);

	}
}

#endif