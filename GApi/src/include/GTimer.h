#ifndef G_TIMER_H
#define G_TIMER_H

#include "StdHeader.h"

namespace GApi {
	namespace Util {

		class DllExport Timer
		{
		public:
			Timer();
			~Timer() {}

			float32 TotalTime()const;  // in seconds
			float32 DeltaTime()const; // in seconds

			void Reset(); // Call before message loop.
			void Start(); // Call when unpaused.
			void Stop();  // Call when paused.
			void Tick();  // Call every frame.

		private:
			double64 mSecondsPerCount;
			double64 mDeltaTime;

			int64 mBaseTime;
			int64 mPausedTime;
			int64 mStopTime;
			int64 mPrevTime;
			int64 mCurrTime;

			bool mStopped;

			bool m_FPSTracking;
			float32 m_FPS;
		};

	}
}



#endif