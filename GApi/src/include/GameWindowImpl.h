#ifndef GAME_WINDOW_H
#define GAME_WINDOW_H

#include "StdHeader.h"
#include "GWindow.h"
#include "IGameWindow.h"
#include "IGameEngine.h"
#include "GTimer.h"

namespace GApi {
	namespace UI {

		using namespace GApi::Util;

		class DllExport GameWindowImpl : implements IWindow
		{
		public:
			GameWindowImpl();
			virtual ~GameWindowImpl(){}

			virtual BOOL Initialize(const WindowOptions& options) override;
			virtual BOOL Start() override;
			virtual void SetWindowTitle(const char8* pszName) = VIRTUAL;
			virtual BOOL SetGameEngine(IGameEngine* pEngine) = VIRTUAL;
			virtual BOOL GetWindowHandle(void* ptr) = VIRTUAL;
			virtual BOOL Cleanup() override;

			virtual BOOL Run(const float32 deltaTime);

			virtual float32 FPS();
			inline bool IsRunning() const;

		protected:

			std::string m_Title;
			bool m_Running;

			IGameEngine* m_pGameEngine;
			Timer m_WindowTimer;
			float32 m_LastFPS;
		};

	}
}


#endif