#ifndef FILE_API_H
#define FILE_API_H

#include "StdHeader.h"

#include <string>
namespace GApi {
	namespace IO {

		DllExport BOOL LoadFile(const char8* pszFilePath, char8* pszFileOutput, uint32 maxLength);

		class Impl;

		class DllExport FileResolver
		{
		private:
			Impl* m_pImpl;

		public:
			FileResolver();
			~FileResolver();

			BOOL SetRootPath(const char8* pszDirectoryPath);
			BOOL GetFilePath(const char8* pszFileName, std::string& outputPath);

			static std::string GetExecutableDirectory();
		};

	}
}

#endif