#ifndef POINT_H
#define POINT_H

#include "StdHeader.h"

namespace GApi {
	namespace GMath {

		// 2D Point.
		// Able to ZeroMemory()
		template <typename T>
		struct DllExport Point2D
		{
			T x;
			T y;
		};

		template<typename T>
		using Vector2DValues = Point2D < T > ;

		// 3D Point.
		// Able to ZeroMemory()
		template <typename T>
		struct DllExport Point3D
		{
			Point3D(){}
			Point3D(T x, T y, T z) { this->x = x; this->y = y; this->z = z; }
			T x;
			T y;
			T z;
		};

		// 4D Point.
		// Able to ZeroMemory()
		template <typename T>
		struct DllExport Point4D
		{
			Point4D(){}
			Point4D(T x, T y, T z, T w) { this->x = x; this->y = y; this->z = z; this->w = w; }
			T x;
			T y;
			T z;
			T w;
		};

		template<typename T>
		using Vector3DValues = Point3D < T >;

		template<typename T>
		using ColorRGB = Point3D < T >;

		template<typename T>
		using Vector4DValues = Point4D < T >;

		template<typename T>
		using ColorRGBA = Point4D < T >;
	}
}
#endif