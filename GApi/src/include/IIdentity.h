#ifndef IDENTITY_INTERFACE_H
#define IDENTITY_INTERFACE_H

#include "StdHeader.h"
#include <string>

namespace GApi {
	namespace Util {

		interface DllExport IIdentity 
		{
			virtual BOOL SetIdentity(const std::string& id = "") = VIRTUAL;
			virtual BOOL GetIdentity(std::string& id) = VIRTUAL;
		};

	}
}

#endif
