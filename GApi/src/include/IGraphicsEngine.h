#ifndef INTERFACE_GRAPHICS_ENGINE_H
#define INTERFACE_GRAPHICS_ENGINE_H

#include "StdHeader.h"
#include "ShaderTypes.h"
#include "GColor.h"
#include "GMesh.h"
#include "GTransform.h"
#include "GTexture.h"
#include "GCamera.h"
#include <memory>

namespace GApi {
	namespace Graphics {

		struct DllExport GraphicsEngineOptions
		{
			bool FullScreen;
			float32 width;
			float32 height;
			ubyte8 swapChainCount;
		};

		typedef uint32 MeshID;
		typedef uint32 TextureID;

		interface DllExport IGraphicsEngine
		{
			virtual ~IGraphicsEngine() {}

			// Window Management
			virtual BOOL OnResize(const float32 width, const float32 height) = VIRTUAL;

			// Options
			virtual BOOL SetOptions(const GraphicsEngineOptions& options) = VIRTUAL;
			virtual BOOL SetWindowHandle(void* ptr) = VIRTUAL;

			// Initialize/Shutdown
			virtual BOOL Startup() = VIRTUAL;
			virtual BOOL Shutdown() = VIRTUAL;

			// Rendering
			virtual BOOL Render() = VIRTUAL;
			virtual BOOL ClearScreen(const RGBAColor& clearColor) = VIRTUAL;

			// Shader Program Functions
			virtual BOOL CreateShaderProgram(const char8* pszProgramName) = VIRTUAL;
			virtual BOOL DeleteShaderProgram(const char8* pszProgramName) = VIRTUAL;
			virtual BOOL AttachShaderToProgram(const char8* pszShaderProgramName, const char8* pszProgramName, ShaderTypesEnum shaderType) = VIRTUAL;

			// Modeling
			virtual BOOL RegisterMesh(Mesh& mesh, MeshID& meshID) = VIRTUAL;
			virtual BOOL DeregisterMesh(const MeshID meshID) = VIRTUAL;
			virtual BOOL CopyMesh(const MeshID meshID, MeshID& newMesh) = VIRTUAL;
			virtual BOOL BindShaderToMesh(const char8* pszProgramName, const MeshID meshID) = VIRTUAL;
			virtual BOOL TransformMesh(const MeshID meshID, const Transform& transformation) = VIRTUAL;

			// Texture
			virtual BOOL RegisterTexture(std::string resourceName, TextureID& textureID) = VIRTUAL;
			virtual BOOL DeregisterTexture(const TextureID textureID) = VIRTUAL;
			virtual BOOL BindTextureToMesh(const TextureID textureID, const MeshID meshID) = VIRTUAL;

			// Camera
			virtual std::shared_ptr<Camera> GetCamera() = VIRTUAL;
			

			virtual std::string GetEngineDescription() const = VIRTUAL;
		};
	}
}
#endif