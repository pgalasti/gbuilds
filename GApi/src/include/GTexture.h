#ifndef G_TEXTURE_H
#define G_TEXTURE_H

#include "StdHeader.h"

#include <vector>
#include <memory>
namespace GApi {
	namespace Graphics {

		interface DllExport ITexture
		{
		public:

			ITexture(){}
			virtual ~ITexture(){}

			virtual BOOL LoadTexture(const char8* pszFileName) = VIRTUAL;
			virtual BOOL ApplyTexture() = VIRTUAL;
			virtual void FreeTexture() = VIRTUAL;

		};

		typedef std::shared_ptr<ITexture> ITexturePtr;
	}
}


#endif
