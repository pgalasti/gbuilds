#ifndef INTERFACE_GAME_WINDOW_H
#define INTERFACE_GAME_WINDOW_H

#include "StdHeader.h"
#include "GWindow.h"
#include "IGameEngine.h"


namespace GApi {
	namespace UI {

		using GApi::GameEngine::IGameEngine;

		interface DllExport IGameWindow : implements IWindow
		{
			virtual ~IGameWindow(){}

			virtual BOOL Initialize(const WindowOptions& options) = VIRTUAL;
			virtual BOOL Cleanup() = VIRTUAL;
			virtual BOOL Start() = VIRTUAL;
			virtual BOOL Run(const float32 deltaTime) = VIRTUAL;

			virtual void SetWindowTitle(const char8* pszName) = VIRTUAL;

			virtual BOOL SetGameEngine(IGameEngine* pEngine) = VIRTUAL;
		};

	}
}

#endif