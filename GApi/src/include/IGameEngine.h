#ifndef INTERFACE_GAME_ENGINE_H
#define INTERFACE_GAME_ENGINE_H

#include "StdHeader.h"

#include "IGraphicsEngine.h"

namespace GApi {
	namespace GameEngine {

		using GApi::Graphics::IGraphicsEngine;
		using GApi::Graphics::GraphicsEngineOptions;

		interface DllExport IGameEngine
		{
			virtual ~IGameEngine(){}

			virtual BOOL Initialize() = VIRTUAL;
			virtual BOOL SetGraphicsEngine(IGraphicsEngine* pGraphicsEngine, const GraphicsEngineOptions& options) = VIRTUAL;
			virtual BOOL Start() = VIRTUAL;
			virtual BOOL UpdateState(const float32 deltaTime) = VIRTUAL;
			virtual BOOL Stop() = VIRTUAL;
			virtual BOOL Pause() = VIRTUAL;
			virtual BOOL Cleanup() = VIRTUAL;
			virtual IGraphicsEngine* GetGraphicsEngine() const = VIRTUAL;
		};

	}
}

#endif