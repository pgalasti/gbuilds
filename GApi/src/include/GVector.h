#ifndef GVECTOR_H
#define GVECTOR_H

#include "StdHeader.h"

#include <math.h>

// SIMD operations
#ifdef GAPI_ENABLE_SIMD
#	ifdef _WIN32
#include <xmmintrin.h>
#	endif
#endif

namespace GApi {
	namespace GMath {

		// 2D Vector.
		// 8 bytes in size if SIMD is enabled. Able to ZeroMemory(). 16 bytes if not
		struct DllExport Vector2D
		{
#ifdef GAPI_ENABLE_SIMD
			union
			{
				struct
				{
					float32 xf;
					float32 yf;
				};
				__m128 sse;
			};

		private:
			Vector2D(__m128 sse) { this->sse = sse; }
		public:
#else
			float32 xf;
			float32 yf;
#endif
			Vector2D() { ZeroMemory(this, sizeof (Vector2D)); };
			Vector2D(float32 x, float32 y) { sse = _mm_set_ps(0.0f, 0.0f, y, x); }
			Vector2D(const Vector2D& otherVector) : xf(otherVector.xf), yf(otherVector.yf){}
			Vector2D& operator= (const Vector2D& otherVector) { xf = otherVector.xf; yf = otherVector.yf; return *this; }

			Vector2D operator+ (const Vector2D& otherVector) const;
			Vector2D operator- (const Vector2D& otherVector) const;
			Vector2D operator/ (const float32 scalar) const;
			Vector2D operator* (const float32 scalar) const;
			const bool operator== (const Vector2D& otherVector) const;

			inline const float32 Magnitude() const;

			Vector2D Normalize() const;
			void NormalizeThis();
			const float32 DotProduct(const Vector2D& otherVector) const;
			const float32 AngleRad(const Vector2D& otherVector) const;
			const float32 AngleDeg(const Vector2D& otherVector) const;
			const bool Orthogonal(const Vector2D& otherVector) const;



			static const float32 Magnitude(const float32 x, const float32 y) { return (float32)sqrt((x*x) + (float32)(y*y)); }
		};

		// 3D Vector.
		// 12 bytes in size. Able to ZeroMemory()
		struct DllExport Vector3D
		{
			float32 xf;
			float32 yf;
			float32 zf;

			Vector3D() { ZeroMemory(this, sizeof (Vector3D)); };
			Vector3D(float32 x, float32 y, float32 z) : xf(x), yf(y), zf(z){}
			Vector3D(const Vector3D& otherVector) : xf(otherVector.xf), yf(otherVector.yf), zf(otherVector.zf){}
			Vector3D& operator= (const Vector3D& otherVector) { xf = otherVector.xf; yf = otherVector.yf; zf = otherVector.zf; return *this; }

			Vector3D operator+ (const Vector3D& otherVector) const;
			Vector3D operator- (const Vector3D& otherVector) const;
			Vector3D operator/ (const float32 scalar) const;
			Vector3D operator* (const float32 scalar) const;
			const bool operator== (const Vector3D& otherVector) const;

			inline const float32 Magnitude() const;

			Vector3D Normalize() const;
			void NormalizeThis();
			const float32 DotProduct(const Vector3D& otherVector) const;
			const float32 AngleRad(const Vector3D& otherVector) const;
			const float32 AngleDeg(const Vector3D& otherVector) const;
			const bool Orthogonal(const Vector3D& otherVector) const;
			const Vector3D Cross(const Vector3D& otherVector) const;


			static const float32 Magnitude(const float32 x, const float32 y, const float32 z) { return (float32)sqrt((x*x) + (y*y) + (z*z)); }
		};
	}
}
#endif
