#ifndef SHADER_TYPES_H
#define SHADER_TYPES_H

#include "PipelineDefs.h"

namespace GApi {
	namespace Graphics {

		enum ShaderTypesEnum
		{
			VertexShader = SHADER_PIPELINE_VERTEX_STAGE,
			FragmentShader = SHADER_PIPELINE_PIXEL_STAGE,
			PixelShader = SHADER_PIPELINE_PIXEL_STAGE,
		};

	}
}
#endif