#ifndef GDEFINES_H
#define GDEFINES_H

#include <stdio.h>
#include <string.h>

#ifdef _WIN32

typedef unsigned __int64 uint64;
typedef signed __int64 int64;
typedef unsigned __int32 uint32;
typedef signed __int32 int32;
typedef unsigned __int16 ushort16;
typedef signed __int16 short16;
typedef __int8 sbyte8;
typedef unsigned __int8 ubyte8;

typedef char char8;
typedef unsigned char uChar8;
typedef long long32;
typedef unsigned long uLong32;
typedef long long long64;
typedef unsigned long long uLong64;
typedef float float32;
typedef double double64;

#else

typedef unsigned long long uint64;
typedef signed long long int64;
typedef unsigned int uint32;
typedef signed int int32;
typedef unsigned short ushort16;
typedef signed short short16;
typedef char sbyte8;
typedef unsigned char ubyte8;

typedef char char8;
typedef unsigned char uChar8;
typedef long long32;
typedef unsigned long uLong32;
typedef long long long64;
typedef unsigned long long uLong64;
typedef float float32;
typedef double double64;

#endif // _WIN32

#ifdef _WIN32

#pragma warning(disable: 4251)
//#define DllExport   __declspec( dllexport ) 
//#define DllImport   __declspec( dllimport ) 

#ifdef _WIN32
	#ifdef _WINDLL
		#define DllExport   __declspec( dllexport ) 
	#else
		#define DllExport   __declspec( dllimport ) 
	#endif
#endif

#define FILE_SEPERATOR_CHAR '\\'
#define FILE_SEPERATOR_STR "\\"

#else
#define DllExport
#define DllImport

#define FILE_SEPERATOR_CHAR '/'
#define FILE_SEPERATOR_STR "/"
#endif

// Cross Platform Common Defines
#ifndef interface
#define interface struct
#endif

#ifndef implements
#define implements public
#endif

#ifndef NULL
#define NULL 0
#endif

#define VIRTUAL 0

#define IsNull(p) p == nullptr
#define IsNotNull(p) p != nullptr

#define SAFE_FREE(p) if(p) { free(p); p = nullptr;}
#define SAFE_DELETE(p) if(p) { delete p; p = nullptr; }
#define C_FILE_CLOSE(p) if(p){fclose(p); p = nullptr;}

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

typedef int BOOL;

#undef ZeroMemory
#ifndef ZeroMemory
#define ZeroMemory(p,s) {memset(p, 0, (s));}
#endif

#define BIT 1
#define BYTE_SIZE 8
#define KILOBYTE_SIZE 1024
#define MEGABYTE_SIZE 1048576
#define GIGABYTE_SIZE 1073741824

#define BIT_1 0b00000001
#define BIT_2 0b00000010
#define BIT_3 0b00000100
#define BIT_4 0b00001000
#define BIT_5 0b00010000
#define BIT_6 0b00100000
#define BIT_7 0b01000000
#define BIT_8 0b10000000

// Class Macros
// Used to configure class structure and disable default behavior.

// Disables implicit default constructor.
#define DISABLE_IMPLICIT_CONSTRUCTOR(Class)                                    \
    Class() = delete;                                                          

// Disables copy constructor.
#define DISABLE_COPY_CONSTRUCTOR(Class)                                        \
    Class(const Class&) = delete;

// Disables assignment operator
#define DISABLE_ASSIGNMENT_OPERATOR(Class)										\
	Class& operator=(const Class&) = delete;

#endif // GDEFINES_H
