#ifndef G_MESH_H
#define G_MESH_H

#include "StdHeader.h"

#include <vector>

#include "GPoint.h"
#include "IIdentity.h"

namespace GApi {
	namespace Graphics {

		using namespace GApi::GMath;
		using namespace GApi::Util;

		typedef Point3D<float32> Position;
		typedef Point2D<float32> UV;
		struct Vertex
		{
			// 0 to 96
			Position position;
			// 96 to 128
			//UV uv;
		};

		class DllExport Mesh : implements IIdentity
		{
		public:

			Mesh();
			Mesh(const Vertex* pVerticies, uint32 numVerticies);
			Mesh(const Vertex* pVerticies, uint32 numVerticies, const ushort16* pIndicies, uint32 numIndicies);
			~Mesh();

			void SetVerticies(const Vertex* pVerticies, uint32 numVerticies);
			void SetIndicies(const ushort16* pIndicies, uint32 numIndicies);

			void GetVerticiesPtr(Vertex** ppVerticies);
			void GetIndiciesPtr(ushort16** ppIndicies);

			std::vector<Vertex> GetVerticies() { return m_Verticies;  }

			uint32 GetNumberVerticies() const;
			uint32 GetNumberIndicies() const;

			virtual BOOL SetIdentity(const std::string& id = "") override;
			virtual BOOL GetIdentity(std::string& id) override;

		private:
			std::vector<Vertex> m_Verticies;
			std::vector<ushort16> m_Indicies;
			std::string m_Id;

		};

	}
}

#endif
