#ifndef G_COLOR_H
#define G_COLOR_H

#include "StdHeader.h"

namespace GApi {
	namespace Graphics {

		struct DllExport RGColor
		{
			RGColor(float32 r, float32 g) { this->r = r; this->g = g; }
			float32 r;
			float32 g;
		};

		struct DllExport RGBColor
		{
			RGBColor(float32 r, float32 g, float32 b) { this->r = r; this->g = g; this->b = b; }
			float32 r;
			float32 g;
			float32 b;
		};

		struct DllExport RGBAColor
		{
			RGBAColor(float32 r, float32 g, float32 b, float32 a) { this->r = r; this->g = g; this->b = b; this->a = a; }
			float32 r;
			float32 g;
			float32 b;
			float32 a;

			static RGBAColor DefaultBlue() 
			{
				return RGBAColor(0.0f, 0.15f, 0.3f, 1.0f);
			}
		};

	}
}

#endif