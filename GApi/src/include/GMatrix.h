#ifndef MATRIX_H
#define MATRIX_H

#include "StdHeader.h"
#include "GMath.h"
#include "GVector.h"


namespace GApi {
	namespace GMath {

		class DllExport Matrix4x4F
		{

		public:
			Matrix4x4F();
			~Matrix4x4F() {}

			// Operators
			Matrix4x4F operator+ (const Matrix4x4F& otherMatrix) const;
			Matrix4x4F operator- (const Matrix4x4F& otherMatrix) const;
			Matrix4x4F operator* (const Matrix4x4F& otherMatrix) const;

			Matrix4x4F operator* (const float32 value) const;
			Matrix4x4F operator/ (const float32 value) const;

			// Evaluation
			/*inline*/ float32 Determinate() const;

			// States
			/*inline*/ void ZeroMatrix();
			/*inline*/ void Transpose();
			/*inline*/ void Identity();
			/*inline*/ void Inverse();

			// Transformations
			/*inline*/ void TranslationMatrix(const float32 x, const float32 y, const float32 z);
			/*inline*/ void TranslationMatrix(const Vector3D& translationVec);
			
			/*inline*/ void ScaleMatrix(const float32 x, const float32 y, const float32 z);
			/*inline*/ void ScaleMatrix(const Vector3D& translationVec);
			
			/*inline*/ void RotateMatrixX(const float32 x);
			/*inline*/ void RotateMatrixY(const float32 y);
			/*inlin*/ void RotateMatrixZ(const float32 z);

			// Static calls
			static void GetIdentityMatrix(Matrix4x4F &matrix);
			static void Add(const Matrix4x4F& matrixA, const Matrix4x4F& matrixB, Matrix4x4F& outMatrix);
			static void Subtract(const Matrix4x4F& matrixA, const Matrix4x4F& matrixB, Matrix4x4F& outMatrix);
			static void Multiply(const Matrix4x4F& matrixA, const Matrix4x4F& matrixB, Matrix4x4F& outMatrix);

			union {
				Values4X4Struct<float32> valuesStruct;
				__m128 rows[4];
			};
			
		};

	}
}

#endif
