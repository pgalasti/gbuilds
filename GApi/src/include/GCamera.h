#ifndef G_CAMERA_H
#define G_CAMERA_H

#include "StdHeader.h"

#include "GMath.h"
#include "GVector.h"
#include "GMatrix.h"

#include <memory>

namespace GApi {
	namespace Graphics {

		using namespace GApi::GMath;

		class Impl;
		
		class DllExport Camera
		{
		protected:

			Impl* m_pImpl;
			void Init();

		public:
			Camera();
			Camera(const Vector3D& position, const Vector3D& focus, const Vector3D& up, float32 aspectRatio, float32 nearPlane, float32 farPlane, float32 fieldOfView = PI_F / 2);
			~Camera();

			enum Major
			{
				Row,
				Column,
			};

			// View Functions
			void SetPosition(const Vector3D& position);
			void SetFocus(const Vector3D& focus);
			void SetUp(const Vector3D& up);

			void GetPosition(Vector3D& position) const;
			void GetFocus(Vector3D& focus) const;
			void GetUp(Vector3D& up) const;

			// Projection Functions
			inline void SetAspectRatio(const float32 aspectRatio);
			inline void SetNearPlane(const float32 nearPlane);
			inline void SetFarPlane(const float32 farPlane);
			inline void SetFieldOfViewRadians(const float32 fieldOfView);

			inline float32 GetAspectRatio() const;
			inline float32 GetNearPlane() const;
			inline float32 GetFarPlane() const;
			inline float32 GetFieldOfViewRadians() const;

			Matrix4x4F GetViewProjectionMatrix(Major major = Row) const;
			Matrix4x4F GetProjectionViewMatrix(Major major = Row) const;
			Matrix4x4F GetViewMatrix(Major major = Row) const;
			Matrix4x4F GetProjectionMatrix(Major major = Row) const;
		
		};

		typedef std::shared_ptr<Camera> CameraPtr;

	}
}

#endif
