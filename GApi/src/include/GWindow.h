#ifndef G_WINDOW_H
#define G_WINDOW_H

#include "StdHeader.h"
#include <string>

namespace GApi {
	namespace UI {

		struct DllExport WindowOptions
		{
			float32 Width;
			float32 Height;
			char8 WindowTitle[256];
		};

		interface DllExport IWindow
		{
			virtual ~IWindow(){}

			virtual BOOL Initialize(const WindowOptions& options) = VIRTUAL;
			virtual BOOL Cleanup() = VIRTUAL;
			virtual BOOL Start() = VIRTUAL;
			virtual BOOL GetWindowHandle(void* ptr) = VIRTUAL;
			
			virtual void SetWindowTitle(const char8* pszName) = VIRTUAL;
		};
	}
}


#endif