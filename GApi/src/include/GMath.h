#ifndef GMATH_HEADER
#define GMATH_HEADER

#include "StdHeader.h"
#include <math.h>
#include "GVector.h"

namespace GApi {
	namespace GMath {

		// Pi
		const float32 PI_F = 3.141592654f;
		// 2 x Pi
		const float32 TWOPI_F = 6.283185307f;
		// 1 / Pi
		const float32 ONEDIVPI_F = 0.318309886f;
		// 1 / 2Pi
		const float32 ONEDIV2PI_F = 0.159154943f;
		// Pi / 2
		const float32 PIDIV2_F = 1.570796327f;
		// Pi / 4
		const float32 PIDIV4_F = 0.785398163f;
		// 180 / Pi
		const float32 RAD_F = 180 / PI_F;

		template <typename T>
		struct DllExport Values2X2Struct
		{
			union
			{
				struct
				{
					T r1c1, r1c2;
					T r2c1, r2c2;
				} Values;

				T ArrValues[4];
			};
		};

		template <typename T>
		struct DllExport Values3X3Struct
		{
			union
			{
				struct
				{
					T r1c1, r1c2, r1c3;
					T r2c1, r2c2, r2c3;
					T r3c1, r3c2, r3c3;
				} Values;

				T ArrValues[9];
			};
		};

		template <typename T>
		struct DllExport Values4X4Struct
		{
			union
			{
				struct
				{
					T r1c1, r1c2, r1c3, r1c4;
					T r2c1, r2c2, r2c3, r2c4;
					T r3c1, r3c2, r3c3, r3c4;
					T r4c1, r4c2, r4c3, r4c4;
				} Values;

				T ArrValues[16];
			};
		};

		template <typename T>
		Values4X4Struct<T> MultiplyScalar(const Values4X4Struct<T>& structure, const T scalar)
		{
			Values4X4Struct<T> result;
			for (register ubyte8 i = 0; i < 16; i++)
				result.ArrValues[i] = structure.ArrValues[i] * scalar;

			return result;
		}

		template <typename T>
		Values4X4Struct<T> DivideScalar(const Values4X4Struct<T>& structure, const T scalar)
		{
			Values4X4Struct<T> result;
			for (register ubyte8 i = 0; i < 16; i++)
				result.ArrValues[i] = structure.ArrValues[i] / scalar;

			return result;
		}

		// There might be a faster algorithm than this. I wrote this myself.
		template <typename T>
		Values4X4Struct<T> Multiply4X4(const Values4X4Struct<T>& matrixA, const Values4X4Struct<T> matrixB)
		{
			/*
			|  0,  1,  2, 3  |   |  0,  1,  2, 3  |
			|  4,  5,  6, 7  |   |  4,  5,  6, 7  |
			|  8,  9, 10, 11 | X |  8,  9, 10, 11 |
			| 12, 13, 14, 15 |   | 12, 13, 14, 15 |
			*/

			Values4X4Struct<T> newMatrix;
			ZeroMemory(&newMatrix, sizeof(Values4X4Struct<T>));

			for (register ubyte8 i = 0; i < 16; i++)
			{
				T value;
				ZeroMemory(&value, sizeof (T));

				ushort16 aStartIndexRow = (i / 4) * 4;
				ushort16 bStartIndexCol = i % 4;
				for (register ubyte8 j = 0; j < 4; j++)
					value += matrixA.ArrValues[aStartIndexRow + j] * matrixB.ArrValues[bStartIndexCol + (j * 4)];

				newMatrix.ArrValues[i] = value;
			}

			return newMatrix;
		}

		template <typename T>
		Values3X3Struct<T> Identity3X3()
		{
			Values3X3Struct<T> identityMatrix;
			ZeroMemory(&identityMatrix, sizeof (Values3X3Struct<T>));

			identityMatrix.Values.r1c1 = 1;
			identityMatrix.Values.r2c2 = 1;
			identityMatrix.Values.r3c3 = 1;

			return identityMatrix;
		}

		template <typename T>
		Values4X4Struct<T> Identity4X4()
		{
			Values4X4Struct<T> identityMatrix;
			ZeroMemory(&identityMatrix, sizeof (Values4X4Struct<T>));

			identityMatrix.Values.r1c1 = 1;
			identityMatrix.Values.r2c2 = 1;
			identityMatrix.Values.r3c3 = 1;
			identityMatrix.Values.r4c4 = 1;

			return identityMatrix;
		}

		template <typename T>
		Values4X4Struct<T> TranslationStruct(const T x, const T y, const T z)
		{
			Values4X4Struct<T> translationMatrix = Identity4X4<T>();
			translationMatrix.Values.r4c1 = x;
			translationMatrix.Values.r4c2 = y;
			translationMatrix.Values.r4c3 = z;

			return translationMatrix;
		}

		template <typename T>
		Values4X4Struct<T> ScaleStruct(const T x, const T y, const T z)
		{
			Values4X4Struct<T> scaleMatrix;
			ZeroMemory(&scaleMatrix, sizeof (Values4X4Struct<T>));

			scaleMatrix.Values.r1c1 = x;
			scaleMatrix.Values.r2c2 = y;
			scaleMatrix.Values.r3c3 = z;
			scaleMatrix.Values.r4c4 = 1;

			return scaleMatrix;
		}

		template <typename T>
		Values4X4Struct<T> RotateStructX(const T x)
		{
			Values4X4Struct<T> rotationMatrix = Identity4X4<T>();
			rotationMatrix.Values.r2c2 = (T)cos(-x);
			rotationMatrix.Values.r2c3 = (T)-sin(-x);
			rotationMatrix.Values.r3c2 = (T)sin(-x);
			rotationMatrix.Values.r3c3 = (T)cos(-x);

			return rotationMatrix;
		}

		template <typename T>
		Values4X4Struct<T> RotateStructY(const T y)
		{
			Values4X4Struct<T> rotationMatrix = Identity4X4<T>();
			rotationMatrix.Values.r1c1 = (T)cos(-y);
			rotationMatrix.Values.r1c3 = (T)sin(-y);
			rotationMatrix.Values.r3c1 = (T)-sin(-y);
			rotationMatrix.Values.r3c3 = (T)cos(-y);

			return rotationMatrix;
		}

		template <typename T>
		Values4X4Struct<T> RotateStructZ(const T z)
		{
			Values4X4Struct<T> rotationMatrix = Identity4X4<T>();
			rotationMatrix.Values.r1c1 = (T)cos(-z);
			rotationMatrix.Values.r1c2 = (T)-sin(-z);
			rotationMatrix.Values.r2c1 = (T)sin(-z);
			rotationMatrix.Values.r2c2 = (T)cos(-z);

			return rotationMatrix;
		}

		template <typename T>
		Values3X3Struct<T> MatrixMinor(const Values4X4Struct<T>& structure, ubyte8 row, ubyte8 column)
		{

			if (row < 1 || row > 4 || column < 1 || column > 4)
				return Identity3X3<T>();

			Values3X3Struct<T> minor;
			ubyte8 minorIndex = 0;
			for (register ubyte8 i = 0; i < 16; i++)
			{
				if ((i % 4) == column-1)
					continue;

				if ((i / 4) == row-1)
					continue;

				minor.ArrValues[minorIndex++] = structure.ArrValues[i];
			}

			return minor;
		}

		template <typename T>
		Values4X4Struct<T> CofactorStruct(const Values4X4Struct<T>& structure)
		{
			Values4X4Struct<T> cofactorStruct;

			for (uint32 iRow = 1; iRow < 5; iRow++)
			{
				for (uint32 iColumn = 1; iColumn < 5; iColumn++)
				{
					Values3X3Struct<T> minor = MatrixMinor(structure, iRow, iColumn);
					uint32 index = iColumn-1 + ((iRow-1)*4);

					cofactorStruct.ArrValues[index] = EvaluateDeterminateStruct(minor) * (float32)pow(-1.0f, iRow + iColumn);
				}
			}

			return cofactorStruct;
		}

		template <typename T>
		Values3X3Struct<T> CofactorStruct(const Values3X3Struct<T>& structure)
		{
			auto cofactor2x2 = [](const Values2X2Struct<T>& struct2x2)
			{
				return (struct2x2.Values.r1c1 * struct2x2.Values.r2c2) - (struct2x2.Values.r1c2 * struct2x2.Values.r2c1);
			};

			Values3X3Struct<T> cofactorStruct;

			Values2X2Struct<T> r1c1;
			r1c1.Values.r1c1 = structure.Values.r2c2;
			r1c1.Values.r1c2 = structure.Values.r2c3;
			r1c1.Values.r2c1 = structure.Values.r3c2;
			r1c1.Values.r2c2 = structure.Values.r3c3;
			cofactorStruct.Values.r1c1 = cofactor2x2(r1c1);

			Values2X2Struct<T> r1c2;
			r1c2.Values.r1c1 = structure.Values.r2c1;
			r1c2.Values.r1c2 = structure.Values.r2c3;
			r1c2.Values.r2c1 = structure.Values.r3c1;
			r1c2.Values.r2c2 = structure.Values.r3c3;
			cofactorStruct.Values.r1c2 = -cofactor2x2(r1c2);

			Values2X2Struct<T> r1c3;
			r1c3.Values.r1c1 = structure.Values.r2c1;
			r1c3.Values.r1c2 = structure.Values.r2c2;
			r1c3.Values.r2c1 = structure.Values.r3c1;
			r1c3.Values.r2c2 = structure.Values.r3c2;
			cofactorStruct.Values.r1c3 = cofactor2x2(r1c3);

			Values2X2Struct<T> r2c1;
			r2c1.Values.r1c1 = structure.Values.r1c2;
			r2c1.Values.r1c2 = structure.Values.r1c3;
			r2c1.Values.r2c1 = structure.Values.r3c2;
			r2c1.Values.r2c2 = structure.Values.r3c3;
			cofactorStruct.Values.r2c1 = -cofactor2x2(r2c1);

			Values2X2Struct<T> r2c2;
			r2c2.Values.r1c1 = structure.Values.r1c1;
			r2c2.Values.r1c2 = structure.Values.r1c3;
			r2c2.Values.r2c1 = structure.Values.r3c1;
			r2c2.Values.r2c2 = structure.Values.r3c3;
			cofactorStruct.Values.r2c2 = cofactor2x2(r2c2);

			Values2X2Struct<T> r2c3;
			r2c3.Values.r1c1 = structure.Values.r1c1;
			r2c3.Values.r1c2 = structure.Values.r1c2;
			r2c3.Values.r2c1 = structure.Values.r3c1;
			r2c3.Values.r2c2 = structure.Values.r3c2;
			cofactorStruct.Values.r2c3 = -cofactor2x2(r2c3);

			Values2X2Struct<T> r3c1;
			r3c1.Values.r1c1 = structure.Values.r1c2;
			r3c1.Values.r1c2 = structure.Values.r1c3;
			r3c1.Values.r2c1 = structure.Values.r2c2;
			r3c1.Values.r2c2 = structure.Values.r2c3;
			cofactorStruct.Values.r3c1 = cofactor2x2(r3c1);

			Values2X2Struct<T> r3c2;
			r3c2.Values.r1c1 = structure.Values.r1c1;
			r3c2.Values.r1c2 = structure.Values.r1c3;
			r3c2.Values.r2c1 = structure.Values.r2c1;
			r3c2.Values.r2c2 = structure.Values.r2c3;
			cofactorStruct.Values.r3c2 = -cofactor2x2(r3c2);

			Values2X2Struct<T> r3c3;
			r3c3.Values.r1c1 = structure.Values.r1c1;
			r3c3.Values.r1c2 = structure.Values.r1c2;
			r3c3.Values.r2c1 = structure.Values.r2c1;
			r3c3.Values.r2c2 = structure.Values.r2c2;
			cofactorStruct.Values.r3c3 = cofactor2x2(r3c3);

			return cofactorStruct;
		}

		template <typename T>
		Values4X4Struct<T> TransposeStruct(const Values4X4Struct<T>& structure)
		{
			Values4X4Struct<T> transposeStruct;
			transposeStruct.Values.r1c1 = structure.Values.r1c1;
			transposeStruct.Values.r2c1 = structure.Values.r1c2;
			transposeStruct.Values.r3c1 = structure.Values.r1c3;
			transposeStruct.Values.r4c1 = structure.Values.r1c4;

			transposeStruct.Values.r1c2 = structure.Values.r2c1;
			transposeStruct.Values.r2c2 = structure.Values.r2c2;
			transposeStruct.Values.r3c2 = structure.Values.r2c3;
			transposeStruct.Values.r4c2 = structure.Values.r2c4;

			transposeStruct.Values.r1c3 = structure.Values.r3c1;
			transposeStruct.Values.r2c3 = structure.Values.r3c2;
			transposeStruct.Values.r3c3 = structure.Values.r3c3;
			transposeStruct.Values.r4c3 = structure.Values.r3c4;

			transposeStruct.Values.r1c4 = structure.Values.r4c1;
			transposeStruct.Values.r2c4 = structure.Values.r4c2;
			transposeStruct.Values.r3c4 = structure.Values.r4c3;
			transposeStruct.Values.r4c4 = structure.Values.r4c4;

			return transposeStruct;
		}

		template <typename T>
		T EvaluateDeterminateStruct(const Values3X3Struct<T>& structure)
		{
			T determinate = 0;

			determinate += (structure.Values.r1c1 * (structure.Values.r2c2 * structure.Values.r3c3));
			determinate -= (structure.Values.r1c1 * (structure.Values.r3c2 * structure.Values.r2c3));

			determinate -= (structure.Values.r1c2 * (structure.Values.r2c1 * structure.Values.r3c3));
			determinate += (structure.Values.r1c2 * (structure.Values.r3c1 * structure.Values.r2c3));

			determinate += (structure.Values.r1c3 * (structure.Values.r2c1 * structure.Values.r3c2));
			determinate -= (structure.Values.r1c3 * (structure.Values.r3c1 * structure.Values.r2c2));

			return determinate;
		}

		template <typename T>
		T EvaluateDeterminateStruct(const Values4X4Struct<T>& structure)
		{
			Values3X3Struct<T> a;
			a.Values.r1c1 = structure.Values.r2c2;
			a.Values.r1c2 = structure.Values.r2c3;
			a.Values.r1c3 = structure.Values.r2c4;

			a.Values.r2c1 = structure.Values.r3c2;
			a.Values.r2c2 = structure.Values.r3c3;
			a.Values.r2c3 = structure.Values.r3c4;

			a.Values.r3c1 = structure.Values.r4c2;
			a.Values.r3c2 = structure.Values.r4c3;
			a.Values.r3c3 = structure.Values.r4c4;

			Values3X3Struct<T> b;
			b.Values.r1c1 = structure.Values.r2c1;
			b.Values.r1c2 = structure.Values.r2c3;
			b.Values.r1c3 = structure.Values.r2c4;

			b.Values.r2c1 = structure.Values.r3c1;
			b.Values.r2c2 = structure.Values.r3c3;
			b.Values.r2c3 = structure.Values.r3c4;

			b.Values.r3c1 = structure.Values.r4c1;
			b.Values.r3c2 = structure.Values.r4c3;
			b.Values.r3c3 = structure.Values.r4c4;

			Values3X3Struct<T> c;
			c.Values.r1c1 = structure.Values.r2c1;
			c.Values.r1c2 = structure.Values.r2c2;
			c.Values.r1c3 = structure.Values.r2c4;

			c.Values.r2c1 = structure.Values.r3c1;
			c.Values.r2c2 = structure.Values.r3c2;
			c.Values.r2c3 = structure.Values.r3c4;

			c.Values.r3c1 = structure.Values.r4c1;
			c.Values.r3c2 = structure.Values.r4c2;
			c.Values.r3c3 = structure.Values.r4c4;

			Values3X3Struct<T> d;
			d.Values.r1c1 = structure.Values.r2c1;
			d.Values.r1c2 = structure.Values.r2c2;
			d.Values.r1c3 = structure.Values.r2c3;

			d.Values.r2c1 = structure.Values.r3c1;
			d.Values.r2c2 = structure.Values.r3c2;
			d.Values.r2c3 = structure.Values.r3c3;

			d.Values.r3c1 = structure.Values.r4c1;
			d.Values.r3c2 = structure.Values.r4c2;
			d.Values.r3c3 = structure.Values.r4c3;

			return (structure.Values.r1c1*EvaluateDeterminateStruct(a)) - (structure.Values.r1c2 * EvaluateDeterminateStruct(b)) + (structure.Values.r1c3 * EvaluateDeterminateStruct(c)) - (structure.Values.r1c4 * EvaluateDeterminateStruct(d));

		}

		template <typename T>
		Values4X4Struct<T> ComputeLookAtMatrixLH(const Vector3D& eyePosition, const Vector3D& at, const Vector3D& up)
		{
			Values4X4Struct<T> result = Identity4X4<T>();

			Vector3D zAxis = at - eyePosition;
			zAxis.NormalizeThis();
			Vector3D xAxis = up.Cross(zAxis);
			xAxis.Normalize();
			Vector3D yAxis = zAxis.Cross(xAxis);

			result.Values.r1c1 = xAxis.xf;
			result.Values.r1c2 = yAxis.xf;
			result.Values.r1c3 = zAxis.xf;
			//result.Values.r1c4 = 0;

			result.Values.r2c1 = xAxis.yf;
			result.Values.r2c2 = yAxis.yf;
			result.Values.r2c3 = zAxis.yf;
			//result.Values.r2c4 = 0;

			result.Values.r3c1 = xAxis.zf;
			result.Values.r3c2 = yAxis.zf;
			result.Values.r3c3 = zAxis.zf;
			//result.Values.r3c4 = 0;

			result.Values.r4c1 = -xAxis.DotProduct(eyePosition);
			result.Values.r4c2 = -yAxis.DotProduct(eyePosition);
			result.Values.r4c3 = -zAxis.DotProduct(eyePosition);
			//result.Values.r4c4 = 1;

			return result;
		}

		template<typename T>
		Values4X4Struct<T> ComputeFOVProjectionLH(const float32 fov, float32 aspect, float32 nearPlane, float32 farPlane)
		{
			Values4X4Struct<T> result = Identity4X4<T>();
			if (fov <= 0 || aspect == 0)
				return result;

			float32 frustrumDepth = farPlane - nearPlane;
			float32 oneOverDepth = 1 / frustrumDepth;
			result.Values.r2c2 = 1 / (float32)tan(0.5f * fov);
			result.Values.r1c1 = result.Values.r2c2 / aspect;
			result.Values.r3c3 = farPlane * oneOverDepth;
			result.Values.r4c3 = (-farPlane * nearPlane) * oneOverDepth;
			result.Values.r3c4 = 1;
			result.Values.r4c4 = 0;

			return result;
		}

		template<typename T>
		T Clamp(const T& x, const T& low, const T& high) { return x < low ? low : (x > high ? high : x); }
		template<typename T>
		T Minimum(const T& a, const T& b) { return a < b ? a : b; }
		template<typename T>
		T Max(const T& a, const T& b) { return a > b ? a : b; }

		// Returns random float in [a, b).
		DllExport const float32 RandF(float32 a, float32 b);
		DllExport const float32 AngleFromXY(float32 x, float32 y);
		DllExport const float32 RandF();
	}
}

#endif
