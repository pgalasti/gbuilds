#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "StdHeader.h"

#include "GMatrix.h"

namespace GApi {
	namespace Graphics {

		using namespace GApi::GMath;

		class Impl;

		class DllExport Transform
		{
		private:
			Impl* m_pImpl;

		public:
			Transform();
			Transform(const Vector3D& translationVec, const Vector3D& rotationVec, const Vector3D& scaleVec);
			~Transform();

			Transform(const Transform& other);
			Transform& operator=(const Transform& other);

			enum Axis
			{
				X,
				Y,
				Z
			};

			Matrix4x4F GetTransformationMatrix() const;

			/*inline*/ void Translate(const Vector3D& translationVec);
			/*inline*/ void Translate(const float32 x, const float32 y, const float32 z);

			/*inline*/ void Rotate(const Vector3D& rotationVec);
			/*inline*/ void Rotate(const float32 deg, Axis axis);

			/*inline*/ void Scale(const Vector3D& scaleVec);
			/*inline*/ void Scale(const float32 x, const float32 y, const float32 z);
		
		};

	}
}

#endif
