#include "GCamera.h"

using namespace GApi::GMath;
using namespace GApi::Graphics;
using GApi::Graphics::Impl;

class Impl
{
public:
	Impl() {}

	Vector3D Postion;
	Vector3D Focus;
	Vector3D Up;

	float32 ScreenAspectRatio;
	float32 NearPlane;
	float32 FarPlane;
	float32 FieldOfViewRadians;
};

Camera::Camera()
{
	m_pImpl = new Impl();
	Init();
}

Camera::Camera(const Vector3D& position, const Vector3D& focus, const Vector3D& up, float32  aspectRatio, float32  nearPlane, float32  farPlane, float32  fieldOfView /*= PI_F / 2*/)
{
	m_pImpl = new Impl();
	Init();

	// View Matrix Properties
	SetPosition(position);
	SetFocus(focus);
	SetUp(up);

	// Projection Matrix Properties
	SetAspectRatio(aspectRatio);
	SetNearPlane(nearPlane);
	SetFarPlane(farPlane);
	SetFieldOfViewRadians(fieldOfView);
}

Camera::~Camera()
{
	SAFE_DELETE(m_pImpl);
}

void Camera::Init()
{

	SetPosition(Vector3D(0.0f, 1.0f, -5.0f));
	SetFocus(Vector3D(0.0f, 1.0f, 0.0f));
	SetUp(Vector3D(0.0f, 1.0f, 0.0f));

	m_pImpl->FieldOfViewRadians = m_pImpl->ScreenAspectRatio = m_pImpl->NearPlane = m_pImpl->FarPlane = 0.0f;
}

void Camera::SetAspectRatio(const float32 aspectRatio) { m_pImpl->ScreenAspectRatio = aspectRatio; }
void Camera::SetNearPlane(const float32 nearPlane) { m_pImpl->NearPlane = nearPlane; }
void Camera::SetFarPlane(const float32 farPlane) { m_pImpl->FarPlane = farPlane; }
void Camera::SetFieldOfViewRadians(const float32 fieldOfView) { m_pImpl->FieldOfViewRadians = fieldOfView; }

float32 Camera::GetAspectRatio() const { return m_pImpl->ScreenAspectRatio; }
float32 Camera::GetNearPlane() const { return m_pImpl->NearPlane; }
float32 Camera::GetFarPlane() const { return m_pImpl->FarPlane; }
float32 Camera::GetFieldOfViewRadians() const { return m_pImpl->FieldOfViewRadians; }

void Camera::SetPosition(const Vector3D& position)
{
	memcpy(&m_pImpl->Postion, &position, sizeof (Vector3D));
}

void Camera::SetFocus(const Vector3D& focus)
{
	memcpy(&m_pImpl->Focus, &focus, sizeof (Vector3D));
}

void Camera::SetUp(const Vector3D& up)
{
	memcpy(&m_pImpl->Up, &up, sizeof (Vector3D));
}

void Camera::GetPosition(Vector3D& position) const
{
	memcpy(&position, &m_pImpl->Postion, sizeof (Vector3D));
}

void Camera::GetFocus(Vector3D& focus) const
{
	memcpy(&focus, &m_pImpl->Focus, sizeof (Vector3D));
}

void Camera::GetUp(Vector3D& up) const
{
	memcpy(&up, &m_pImpl->Up, sizeof (Vector3D));
}

Matrix4x4F Camera::GetViewProjectionMatrix(Major major/* = Row*/) const
{
	return GetViewMatrix(major)*GetProjectionMatrix(major);
}

Matrix4x4F Camera::GetProjectionViewMatrix(Major major/* = Row*/) const
{
	return GetProjectionMatrix(major)*GetViewMatrix(major);
}

Matrix4x4F Camera::GetViewMatrix(Major major/* = Row*/) const
{
	Matrix4x4F result;
	result.valuesStruct = ComputeLookAtMatrixLH<float32>(m_pImpl->Postion, m_pImpl->Focus, m_pImpl->Up);
	if (major == Column)
		result.Transpose();

	return result;
}

Matrix4x4F Camera::GetProjectionMatrix(Major major/* = Row*/) const
{
	Matrix4x4F result;
	result.valuesStruct = ComputeFOVProjectionLH<float32>(m_pImpl->FieldOfViewRadians, m_pImpl->ScreenAspectRatio, m_pImpl->NearPlane, m_pImpl->FarPlane);
	if (major == Column)
		result.Transpose();

	return result;
}
