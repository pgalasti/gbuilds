#include "GMesh.h"

using namespace GApi::Graphics;

Mesh::Mesh()
{

}

Mesh::Mesh(const Vertex* pVerticies, uint32 numVerticies)
{
	this->SetVerticies(pVerticies, numVerticies);
}

Mesh::Mesh(const Vertex* pVerticies, uint32 numVerticies, const ushort16* pIndicies, uint32 numIndicies)
{
	this->SetVerticies(pVerticies, numVerticies);
	this->SetIndicies(pIndicies, numIndicies);
}

Mesh::~Mesh()
{

}

void Mesh::SetVerticies(const Vertex* pVerticies, uint32 numVerticies)
{
	m_Verticies.reserve(numVerticies);

	Vertex vertex;
	for (uint32 i = 0; i < numVerticies; i++)
	{
		memcpy(&vertex, pVerticies + i, sizeof(Vertex));
		m_Verticies.push_back(vertex);
	}
}

void Mesh::SetIndicies(const ushort16* pIndicies, uint32 numIndicies)
{
	m_Indicies.reserve(numIndicies);

	for (uint32 i = 0; i < numIndicies; i++)
		m_Indicies.push_back(*(pIndicies + i));
}

void Mesh::GetVerticiesPtr(Vertex** ppVerticies)
{
	*ppVerticies = new Vertex[GetNumberVerticies()];
	memcpy(*ppVerticies, &m_Verticies[0], sizeof(Vertex)*GetNumberVerticies());
}

void Mesh::GetIndiciesPtr(ushort16** ppIndicies)
{
	*ppIndicies = &m_Indicies[0];
}

uint32 Mesh::GetNumberVerticies() const
{
	return m_Verticies.size();
}

uint32 Mesh::GetNumberIndicies() const
{
	return m_Indicies.size();
}

BOOL Mesh::SetIdentity(const std::string& id/* = ""*/)
{
	std::string idToUse = id;

	if (id.empty()) {
		
		// Get a cross platform UUID to generate this
		static uint64 staticId = 0;
	
		idToUse = std::to_string(++staticId);
	}

	m_Id = idToUse;

	return TRUE;
}

BOOL Mesh::GetIdentity(std::string& id)
{

	id = m_Id;
	return TRUE;
}
