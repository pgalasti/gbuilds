#include "GTransform.h"

using namespace GApi::Graphics;
using namespace GApi::GMath;
using GApi::Graphics::Impl;

class Impl
{
public:
	Impl() {}

	Vector3D TranslationVec;
	Vector3D RotationVec;
	Vector3D ScaleVec;
};

Transform::Transform() : m_pImpl(new Impl())
{
	ZeroMemory(&m_pImpl->TranslationVec, sizeof (Vector3D));
	ZeroMemory(&m_pImpl->RotationVec, sizeof (Vector3D));
	this->Scale(1.0, 1.0, 1.0);
}

Transform::Transform(const Vector3D& translationVec, const Vector3D& rotationVec, const Vector3D& scaleVec) : m_pImpl(new Impl())
{
	this->Translate(translationVec);
	this->Rotate(rotationVec);
	this->Scale(scaleVec);
}

Transform::~Transform()
{
	SAFE_DELETE(m_pImpl);
}

Transform::Transform(const Transform& other) 
{
	m_pImpl = new Impl();
	memcpy(m_pImpl, other.m_pImpl, sizeof (Impl));
}

Transform& Transform::operator=(const Transform& other)
{
	m_pImpl = new Impl();
	memcpy(m_pImpl, other.m_pImpl, sizeof(Impl));
	return *this;
}

Matrix4x4F Transform::GetTransformationMatrix() const
{
	Matrix4x4F translationMatrix;
	translationMatrix.Identity();
	translationMatrix.TranslationMatrix(m_pImpl->TranslationVec);

	Matrix4x4F scaleMatrix;
	scaleMatrix.Identity();
	scaleMatrix.ScaleMatrix(m_pImpl->ScaleVec);

	Matrix4x4F rotationMatrix;
	rotationMatrix.Identity();
	rotationMatrix.RotateMatrixX(m_pImpl->RotationVec.xf);
	rotationMatrix.RotateMatrixY(m_pImpl->RotationVec.yf);
	rotationMatrix.RotateMatrixZ(m_pImpl->RotationVec.zf);

	return translationMatrix * rotationMatrix * scaleMatrix;
}

void Transform::Translate(const Vector3D& translationVec)
{
	m_pImpl->TranslationVec = translationVec;
}

void Transform::Translate(const float32 x, const float32 y, const float32 z)
{
	m_pImpl->TranslationVec.xf = x;
	m_pImpl->TranslationVec.yf = y;
	m_pImpl->TranslationVec.zf = z;
}

void Transform::Rotate(const Vector3D& rotationVec)
{
	m_pImpl->RotationVec = rotationVec;
}

void Transform::Rotate(const float32 deg, Axis axis)
{
	switch (axis)
	{
	case X:
		m_pImpl->RotationVec.xf = deg;
		break;
	case Y:
		m_pImpl->RotationVec.yf = deg;
		break;
	case Z:
		m_pImpl->RotationVec.zf = deg;
		break;
	}
	
}

void Transform::Scale(const Vector3D& scaleVec)
{
	m_pImpl->ScaleVec = scaleVec;
}

void Transform::Scale(const float32 x, const float32 y, const float32 z)
{
	m_pImpl->ScaleVec.xf = x;
	m_pImpl->ScaleVec.yf = y;
	m_pImpl->ScaleVec.zf = z;
}
