#include "GameWindowImpl.h"

#ifdef _WIN32
#include "Windows.h"
#endif

using namespace GApi::UI;

GameWindowImpl::GameWindowImpl() 
{
	int a = 0;
	m_Running = false;
	m_pGameEngine = nullptr;
	m_LastFPS = 0.0f;
}

BOOL GameWindowImpl::Initialize(const WindowOptions& options)
{
	return TRUE;
}

BOOL GameWindowImpl::Start()
{
	m_Running = true;
	
	if (!m_pGameEngine->Start())
		return FALSE;

	m_WindowTimer.Start();
	m_WindowTimer.Reset();
	do
	{
		m_WindowTimer.Tick();
		Run(m_WindowTimer.DeltaTime());
	} while (m_Running);

	return TRUE;
}

BOOL GameWindowImpl::Cleanup() 
{
	if (!m_pGameEngine->Stop())
		return FALSE;

	if (!m_pGameEngine->Cleanup())
		return FALSE;

	return TRUE;
}

BOOL GameWindowImpl::Run(const float32 deltaTime)
{
	if (!m_pGameEngine->UpdateState(deltaTime))
		return FALSE;

	const float32 fps = FPS();

	static float32 deltaTimeUpdate = 0.0f;
	deltaTimeUpdate += deltaTime;
	if (deltaTimeUpdate > 1.0f)
	{
		deltaTimeUpdate = 0.0f;
		char pszTitle[128];
		strcpy(pszTitle, m_Title.c_str());
		sprintf(pszTitle, "Test - FPS: %d", (uint32)fps);

		this->SetWindowTitle(pszTitle);
	}
	
	return TRUE;
}

float32 GameWindowImpl::FPS()
{
	static float lastFPS = 0.0f;
	static int frameCount = 0;
	static float timeElapsed = 0.0f;
	static float lastTime = 0.0f;

	float testFPS = (m_WindowTimer.TotalTime() - lastTime) * 1000;
	frameCount++;

	lastTime = m_WindowTimer.TotalTime();

	if ((m_WindowTimer.TotalTime() - timeElapsed) >= 1.0f)
	{
		float fps = (float)frameCount;
		float mspf = 1000.0f / fps;

		frameCount = 0;
		timeElapsed += 1.0f;
		m_LastFPS = fps;
	}


	return m_LastFPS;
}

bool GameWindowImpl::IsRunning() const
{
	return m_Running;
}
