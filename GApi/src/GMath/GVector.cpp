#include "StdHeader.h"
#include "GVector.h"
#include "GMath.h"

using GApi::GMath::Vector2D;

Vector2D Vector2D::operator+ (const Vector2D& otherVector)  const
{
	return Vector2D(this->xf + otherVector.xf, this->yf + otherVector.yf);
}

Vector2D Vector2D::operator- (const Vector2D& otherVector) const
{
	return Vector2D(this->xf - otherVector.xf, this->yf - otherVector.yf);
}

Vector2D Vector2D::operator/ (const float32  scalar) const
{
	return Vector2D(this->xf / scalar, this->yf / scalar);
}

Vector2D Vector2D::operator* (const float32  scalar) const
{
	return Vector2D(this->xf*scalar, this->yf*scalar);
}

const bool Vector2D::operator== (const Vector2D& otherVector) const
{
	return (this->xf == otherVector.xf && this->yf == otherVector.yf);
}

const float32  Vector2D::Magnitude() const
{
	return (float32)sqrt((xf*xf) + (yf*yf));
}

Vector2D Vector2D::Normalize() const
{
	float32 fMagnitude = Magnitude();
#ifdef f
	return Vector2D(_mm_div_ps(sse, _mm_set_ss(fMagnitude)));
#else 
	return Vector2D(xf / fMagnitude, yf / fMagnitude);
#endif
}

void Vector2D::NormalizeThis()
{
	const float32  magnitude = Magnitude();
	xf /= magnitude;
	yf /= magnitude;
}

const float32  Vector2D::DotProduct(const Vector2D& otherVector) const
{
	return (this->xf * otherVector.xf) + (this->yf * otherVector.yf);
}

const float32  Vector2D::AngleRad(const Vector2D& otherVector) const
{
	return (float32)acos(DotProduct(otherVector) / (this->Magnitude() * otherVector.Magnitude()));
}

const float32  Vector2D::AngleDeg(const Vector2D& otherVector) const
{
	return (float32)acos(DotProduct(otherVector) / (this->Magnitude() * otherVector.Magnitude()))
		* RAD_F;
}

const bool Vector2D::Orthogonal(const Vector2D& otherVector) const
{
	return DotProduct(otherVector) == 0.0;
}

// Vector3D
using GApi::GMath::Vector3D;

Vector3D Vector3D::operator+ (const Vector3D& otherVector) const
{
	return Vector3D(this->xf + otherVector.xf, this->yf + otherVector.yf, this->zf + otherVector.zf);
}

Vector3D Vector3D::operator- (const Vector3D& otherVector) const
{
	return Vector3D(this->xf - otherVector.xf, this->yf - otherVector.yf, this->zf - otherVector.zf);
}

Vector3D Vector3D::operator/ (const float32  scalar) const
{
	return Vector3D(this->xf / scalar, this->yf / scalar, this->zf / scalar);
}

Vector3D Vector3D::operator* (const float32  scalar) const
{
	return Vector3D(this->xf*scalar, this->yf*scalar, this->zf*scalar);
}

const bool Vector3D::operator== (const Vector3D& otherVector) const
{
	return (this->xf == otherVector.xf && this->yf == otherVector.yf && this->zf == otherVector.zf);
}

inline const float32  Vector3D::Magnitude() const
{
	return (float32)sqrt((xf*xf) + (yf*yf) + (zf*zf));
}

Vector3D Vector3D::Normalize() const
{
	const float32  magnitude = Magnitude();
	if (magnitude == 0.0f)
		return Vector3D(0, 0, 0);

	return Vector3D(xf / magnitude, yf / magnitude, zf / magnitude);
}

void Vector3D::NormalizeThis()
{
	const float32  magnitude = Magnitude();
	if (magnitude == 0.0f)
	{
		xf = 0.0f;
		yf = 0.0f;
		zf = 0.0f;
		return;
	}

	xf /= magnitude;
	yf /= magnitude;
	zf /= magnitude;
}

const float32  Vector3D::DotProduct(const Vector3D& otherVector) const
{
	return (this->xf * otherVector.xf) + (this->yf * otherVector.yf) + (this->zf * otherVector.zf);
}

const float32  Vector3D::AngleRad(const Vector3D& otherVector) const
{
	return (float32)acos(DotProduct(otherVector) / (this->Magnitude()*otherVector.Magnitude()));
}

const float32  Vector3D::AngleDeg(const Vector3D& otherVector) const
{
	return (float32)acos(DotProduct(otherVector) / (this->Magnitude()*otherVector.Magnitude()))
		*RAD_F;
}

const bool Vector3D::Orthogonal(const Vector3D& otherVector) const
{
	return DotProduct(otherVector) == 0.0;
}

const Vector3D Vector3D::Cross(const Vector3D& otherVector) const
{
	return Vector3D(
		((this->yf*otherVector.zf) - (this->zf*otherVector.yf)),
		((this->zf*otherVector.xf) - (this->xf*otherVector.zf)),
		((this->xf*otherVector.yf) - (this->yf*otherVector.xf))
		);
}