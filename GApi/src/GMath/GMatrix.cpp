#include "StdHeader.h"
#include "GMatrix.h"
#include "GVector.h"

using GApi::GMath::Matrix4x4F;
using GApi::GMath::Vector3D;

Matrix4x4F::Matrix4x4F()
{
	ZeroMatrix();
}

void Matrix4x4F::ZeroMatrix()
{
	ZeroMemory(&this->valuesStruct, sizeof(Values4X4Struct<float32 >));
}

void Matrix4x4F::Transpose()
{
	this->valuesStruct = TransposeStruct<float32 >(this->valuesStruct);
}

void Matrix4x4F::Identity()
{
	this->valuesStruct = Identity4X4<float32 >();
}

void Matrix4x4F::Inverse()
{
	const float32  DET = this->Determinate();

	if (DET == 0.0f)
		return;

	Values4X4Struct<float32 > cofactorMatrix = CofactorStruct<float32 >(this->valuesStruct);
	Values4X4Struct<float32 > cofactorTransposeMatrix = TransposeStruct<float32 >(cofactorMatrix);
	
	this->valuesStruct = DivideScalar(cofactorTransposeMatrix, DET);
}

float32  Matrix4x4F::Determinate() const
{
	return EvaluateDeterminateStruct<float32 >(this->valuesStruct);
}

void Matrix4x4F::TranslationMatrix(const float32  x, const float32  y, const float32  z)
{
	this->Identity();

	this->valuesStruct.Values.r1c4 = x;
	this->valuesStruct.Values.r2c4 = y;
	this->valuesStruct.Values.r3c4 = z;
}

void Matrix4x4F::TranslationMatrix(const Vector3D& translationVec)
{
	this->TranslationMatrix(translationVec.xf, translationVec.yf, translationVec.zf);
}

void Matrix4x4F::ScaleMatrix(const float32  x, const float32  y, const float32  z)
{
	this->Identity();

	this->valuesStruct.Values.r1c1 = x;
	this->valuesStruct.Values.r2c2 = y;
	this->valuesStruct.Values.r3c3 = z;
}

void Matrix4x4F::ScaleMatrix(const Vector3D& translationVec)
{
	this->ScaleMatrix(translationVec.xf, translationVec.yf, translationVec.zf);
}

void Matrix4x4F::RotateMatrixX(const float32  x)
{
	this->valuesStruct = RotateStructX<float32 >(x);
}

void Matrix4x4F::RotateMatrixY(const float32  y)
{
	this->valuesStruct = RotateStructY<float32 >(y);
}

void Matrix4x4F::RotateMatrixZ(const float32  z)
{
	this->valuesStruct = RotateStructZ<float32 >(z);
}

Matrix4x4F Matrix4x4F::operator+ (const Matrix4x4F& otherMatrix)  const
{
	Matrix4x4F resultMatrix;
	Matrix4x4F::Add(*this, otherMatrix, resultMatrix);

	return resultMatrix;
}

Matrix4x4F Matrix4x4F::operator- (const Matrix4x4F& otherMatrix)  const
{
	Matrix4x4F resultMatrix;
	Matrix4x4F::Subtract(*this, otherMatrix, resultMatrix);

	return resultMatrix;
}

Matrix4x4F Matrix4x4F::operator* (const Matrix4x4F& otherMatrix)  const
{
	Matrix4x4F resultMatrix;
	Matrix4x4F::Multiply(*this, otherMatrix, resultMatrix);

	return resultMatrix;
}

Matrix4x4F Matrix4x4F::operator* (const float32  value) const
{
	Matrix4x4F resultMatrix;

	if (value == 0)
	{
		resultMatrix.ZeroMatrix();
		return resultMatrix;
	}

	resultMatrix.valuesStruct = MultiplyScalar(this->valuesStruct, value);

	return resultMatrix;
}

Matrix4x4F Matrix4x4F::operator/ (const float32  value) const
{
	Matrix4x4F resultMatrix;

	if (value == 0)
	{
		resultMatrix.ZeroMatrix();
		return resultMatrix;
	}

	resultMatrix.valuesStruct = DivideScalar(this->valuesStruct, value);

	return resultMatrix;
}

/*static */void Matrix4x4F::GetIdentityMatrix(Matrix4x4F &matrix)
{
	matrix.valuesStruct = Identity4X4<float32 >();
}

/*static */void Matrix4x4F::Add(const Matrix4x4F& matrixA, const Matrix4x4F& matrixB, Matrix4x4F& outMatrix)
{
	outMatrix.ZeroMatrix();
	for (register ubyte8 i = 0; i < 16; i++)
		outMatrix.valuesStruct.ArrValues[i] = matrixA.valuesStruct.ArrValues[i] + matrixB.valuesStruct.ArrValues[i];
}

/*static */void Matrix4x4F::Subtract(const Matrix4x4F& matrixA, const Matrix4x4F& matrixB, Matrix4x4F& outMatrix)
{
	outMatrix.ZeroMatrix();
	for (register ubyte8 i = 0; i < 16; i++)
		outMatrix.valuesStruct.ArrValues[i] = matrixA.valuesStruct.ArrValues[i] - matrixB.valuesStruct.ArrValues[i];
}

/*static */void Matrix4x4F::Multiply(const Matrix4x4F& matrixA, const Matrix4x4F& matrixB, Matrix4x4F& outMatrix)
{
	outMatrix.ZeroMatrix();
	outMatrix.valuesStruct = Multiply4X4<float32 >(matrixA.valuesStruct, matrixB.valuesStruct);
}