#include "GMath.h"
#include <math.h>
#include <stdlib.h>

namespace GApi {
	namespace GMath {

		const float32 AngleFromXY(float32 x, float32 y)
		{
			float32 theta = 0.0f;

			// Quadrant I or IV
			if (x >= 0.0f)
			{
				// If x = 0, then atanf(y/x) = +pi/2 if y > 0
				//                atanf(y/x) = -pi/2 if y < 0
				theta = atanf(y / x); // in [-pi/2, +pi/2]

				if (theta < 0.0f)
					theta += 2.0f*PI_F; // in [0, 2*pi).
			}

			// Quadrant II or III
			else
				theta = atanf(y / x) + PI_F; // in [0, 2*pi).

			return theta;
		}

		const float32 RandF()
		{
			return (float32)(rand()) / (float32)RAND_MAX;
		}

		// Returns random float32 in [a, b).
		const float32 RandF(float32 a, float32 b)
		{
			return a + RandF()*(b - a);
		}

	}
}