#include "DirectXMesh.h"
#include "d3dcompiler.h"
#include <wrl/client.h>
#include "D3DUtil.h"

using GApi::DirectX::Engine::DirectXMesh;

using GApi::DirectX::Utility::CreateDefaultBuffer;

DirectXMesh::DirectXMesh(Mesh& mesh)
{
	m_VertexCount = mesh.GetNumberVerticies();
	m_IndexCount = mesh.GetNumberIndicies();

	m_VertexByteStride = sizeof(Vertex);

	m_VertexByteSize = m_VertexCount * m_VertexByteStride;
	m_IndexByteSize = m_IndexCount * sizeof(short16);

	mesh.GetVerticiesPtr(&m_pVerticies);
	mesh.GetIndiciesPtr(&m_pIndicies);
}

DirectXMesh::~DirectXMesh()
{
	m_pVertexBufferCPU.Reset();
	m_pIndexBufferCPU.Reset();

	m_pVertexBufferGPU.Reset();
	m_pVertexUploadBufferGPU.Reset();

	m_pIndexBufferGPU.Reset();
	m_pIndexUploadBufferGPU.Reset();
}

BOOL DirectXMesh::Initialize(ID3D12Device* pDevice, ID3D12GraphicsCommandList* pCommandList)
{

	if (FAILED(D3DCreateBlob((SIZE_T)m_VertexByteSize, &m_pVertexBufferCPU)))
	{
		// Log
		return FALSE;
	}
	memcpy(m_pVertexBufferCPU->GetBufferPointer(), m_pVerticies, m_VertexByteSize);

	if (FAILED(D3DCreateBlob((SIZE_T)m_IndexByteSize, &m_pIndexBufferCPU)))
	{
		// Log
		return FALSE;
	}
	memcpy(m_pIndexBufferCPU->GetBufferPointer(), m_pIndicies, m_IndexByteSize);

	if (!CreateDefaultBuffer(pDevice, pCommandList, m_pVerticies, (UINT64)m_VertexByteSize, m_pVertexUploadBufferGPU, m_pVertexBufferGPU))
	{
		// Log
		return FALSE;
	}

	if (!CreateDefaultBuffer(pDevice, pCommandList, m_pIndicies, (UINT64)m_IndexByteSize, m_pIndexUploadBufferGPU, m_pIndexBufferGPU))
	{
		// Log
		return FALSE;
	}

	return TRUE;
}

D3D12_VERTEX_BUFFER_VIEW DirectXMesh::GetVertexBufferView() const
{
	D3D12_VERTEX_BUFFER_VIEW vbv;
	vbv.BufferLocation = m_pVertexBufferGPU->GetGPUVirtualAddress();
	vbv.StrideInBytes = m_VertexByteStride;
	vbv.SizeInBytes = m_VertexByteSize;

	return vbv;
}

D3D12_INDEX_BUFFER_VIEW DirectXMesh::GetIndexBufferView() const
{
	D3D12_INDEX_BUFFER_VIEW ibv;
	ibv.BufferLocation = m_pIndexBufferGPU->GetGPUVirtualAddress();
	ibv.Format = DXGI_FORMAT_R16_UINT; // For now
	ibv.SizeInBytes = m_IndexByteSize;

	return ibv;
}