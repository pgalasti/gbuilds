#include "Win32Window.h"

using GApi::DirectX::Window::Win32GameWindow;
using GApi::Graphics::IGraphicsEngine;
using GApi::Graphics::GraphicsEngineOptions;

static Win32GameWindow* WindowInstance = nullptr;

Win32GameWindow::Win32GameWindow(HINSTANCE hInstance) : GameWindowImpl()
{
	WindowInstance = this;
	
	m_hAppInstance = hInstance;
	m_Resizing = false;
}

Win32GameWindow::~Win32GameWindow() 
{
}

LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	return Win32GameWindow::GetStaticInstance()->MsgProc(hwnd, msg, wParam, lParam);
}

Win32GameWindow * Win32GameWindow::GetStaticInstance()
{
	return WindowInstance;
}

LRESULT Win32GameWindow::MsgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	IGraphicsEngine* pGraphicsEngine = m_pGameEngine->GetGraphicsEngine();
	switch (msg)
	{
	case WM_ACTIVATE:
		if (LOWORD(wParam) == WA_INACTIVE)
		{
		}
		else
		{
		}
		return 0;

	case WM_ENTERSIZEMOVE:
		m_Resizing = true;
		return 0;

	case WM_EXITSIZEMOVE:
		m_Resizing = false;

		if (!pGraphicsEngine->OnResize(m_WindowOptions.Width, m_WindowOptions.Height))
		{
			// Log
			return -1;
		}

		return 0;

	case WM_SIZE:
		m_WindowOptions.Width = (float32)LOWORD(lParam);
		m_WindowOptions.Height = (float32)HIWORD(lParam);

		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	case WM_MENUCHAR:
		return MAKELRESULT(0, MNC_CLOSE);

	case WM_GETMINMAXINFO:
		return 0;

	case WM_LBUTTONDOWN:
	case WM_MBUTTONDOWN:
	case WM_RBUTTONDOWN:
		return 0;
	case WM_LBUTTONUP:
	case WM_MBUTTONUP:
	case WM_RBUTTONUP:
		return 0;
	case WM_MOUSEMOVE:
		return 0;
	case WM_KEYUP:
		if (wParam == VK_ESCAPE)
		{
			PostQuitMessage(0);
		}

		return 0;
	}

	return DefWindowProc(hwnd, msg, wParam, lParam);
}

BOOL Win32GameWindow::Initialize(const WindowOptions & options)
{
	if (!GameWindowImpl::Initialize(options))
		return FALSE;
	
	m_WindowOptions = options;

	WNDCLASS wc;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = MainWndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = m_hAppInstance;
	wc.hIcon = LoadIcon(0, IDI_APPLICATION);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
	wc.lpszMenuName = 0;
	wc.lpszClassName = L"MainWnd";

	if (!RegisterClass(&wc))
	{
		MessageBox(0, L"RegisterClass Failed.", 0, 0);
		return false;
	}

	// Compute window rectangle dimensions based on requested client area dimensions.
	RECT R = { 0, 0, (LONG)m_WindowOptions.Width, (LONG)m_WindowOptions.Height };
	AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
	int width = R.right - R.left;
	int height = R.bottom - R.top;

	m_hMainWnd = CreateWindow(L"MainWnd", L"Window Caption",
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, width, height, 0, 0, m_hAppInstance, 0);
	if (!m_hMainWnd)
	{
		auto lastError = GetLastError();
		MessageBox(0, L"CreateWindow Failed.", 0, 0);
		return false;
	}

	ShowWindow(m_hMainWnd, SW_SHOW);
	UpdateWindow(m_hMainWnd);

	return TRUE;

}

BOOL Win32GameWindow::Cleanup()
{
	if (!GameWindowImpl::Cleanup())
		return FALSE;

	return TRUE;
}

BOOL Win32GameWindow::Start()
{
	if (!GameWindowImpl::Start())
		return FALSE;

	return TRUE;
}

BOOL Win32GameWindow::Run(const float32 deltaTime)
{
	static float32 stateUpdateDeltaTime = 0.0f;
	stateUpdateDeltaTime += deltaTime;

	MSG msg = { 0 };
	if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	else
	{
		if (!GameWindowImpl::Run(stateUpdateDeltaTime))
			return FALSE;
		stateUpdateDeltaTime = 0.0f;
	}

	if (msg.message == WM_QUIT)
	{
		
		m_Running = false;
	}

	return TRUE;
}

void Win32GameWindow::SetWindowTitle(const char8 * pszName)
{
	this->m_Title = pszName;
	
	uint32 size = MultiByteToWideChar(CP_UTF8, 0, pszName, strlen(pszName), nullptr, 0);
	std::wstring wstrTo(size, 0);
	MultiByteToWideChar(CP_UTF8, 0, pszName, strlen(pszName), &wstrTo[0], size);
	SetWindowText(m_hMainWnd, wstrTo.c_str());
}

BOOL Win32GameWindow::SetGameEngine(IGameEngine* pEngine)
{
	m_pGameEngine = pEngine;
	return TRUE;
}

BOOL Win32GameWindow::GetWindowHandle(void* ptr)
{
	memcpy(ptr, &m_hMainWnd, sizeof(HWND));
	return TRUE;
}