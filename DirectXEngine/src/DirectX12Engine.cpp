#include "DirectX12Engine.h"
#include "D3dx12.h"
#include "D3DUtil.h"
#include "GStringUtils.h"

using namespace GApi::DirectX::Engine;

DirectX12Engine::DirectX12Engine()
{
	ZeroMemory(&m_GraphicsOptions, sizeof(GraphicsEngineOptions));
	m_bRunning = false;
	m_pCamera = nullptr;
	m_pMeshIDGenerator = nullptr;

	m_RtvDescriptorSize = 0;
	m_DsvDescriptorSize = 0;
	m_CbvSrvDescriptorSize = 0;

	m_CurrentFence = 0;
	m_CurrentBackBuffer = 0;

	m_pDXGIFactory = nullptr;
	m_pDevice = nullptr;
	m_pFence = nullptr;
	m_pCommandQueue = nullptr;
	m_pCommandAllocator = nullptr;
	m_pCommandList = nullptr;
	m_pSwapChain = nullptr;

	m_pRtvHeap = nullptr;
	m_pDsvHeap = nullptr;

	m_pSwapChainBuffer[0] = nullptr;
	m_pSwapChainBuffer[1] = nullptr;
	m_pSwapChainBuffer[2] = nullptr;
	
	m_pDepthStencilBuffer = nullptr;
}

BOOL DirectX12Engine::SetOptions(const GraphicsEngineOptions& options)
{
	m_GraphicsOptions = options;
	return TRUE;
}

BOOL DirectX12Engine::SetWindowHandle(void* ptr)
{
	memcpy(&m_hWnd, ptr, sizeof(HWND));
	return TRUE;
}

BOOL DirectX12Engine::Startup()
{
	m_pCamera = std::make_shared<Camera>();
	m_pMeshIDGenerator = new IDGenerator<MeshID>(1);

#ifdef GAPI_DEBUG
	// Enable the D3D12 debug layer.
	{
		if (FAILED((D3D12GetDebugInterface(IID_PPV_ARGS(&m_pDebugController)))))
		{
			// Log
			return FALSE;
		}
		m_pDebugController->EnableDebugLayer();
		
	}
#endif

	if (!CreateDXGIFactory())
		return FALSE;

	if (!CreateDeviceAndFence())
		return FALSE;

	UINT qualityLevels = 0;
	if (!CheckMultisampleLevels(qualityLevels))
		return FALSE;

	if (!CreateQueueAllocatorCommands())
		return FALSE;

	if (!CreateSwapChain(0))
		return FALSE;

	if (!CreateDescriptorHeaps())
		return FALSE;

	m_bRunning = true;

	if (!OnResize(m_GraphicsOptions.width, m_GraphicsOptions.height))
		return FALSE;

	if (FAILED(m_pCommandList->Reset(m_pCommandAllocator.Get(), nullptr)))
		return FALSE;

	if (!CreateConstantBuffers())
		return FALSE;

	if (!CreateRootSignature())
		return FALSE;

	if (!CreateShaderProgram("TestShader"))
		return FALSE;

	if (!AttachShaderToProgram("C:\\Users\\Paul\\Source\\Repos\\gbuilds\\DirectXEngine\\src\\DefaultShaders\\color.hlsl", "TestShader", VertexShader))
		return FALSE;
	if (!AttachShaderToProgram("C:\\Users\\Paul\\Source\\Repos\\gbuilds\\DirectXEngine\\src\\DefaultShaders\\color.hlsl", "TestShader", PixelShader))
		return FALSE;

	if (!CreatePSO())
		return FALSE;

	if (!ExecuteCommandLists())
		return FALSE;

	if (!FlushCommandQueue())
		return FALSE;

	return TRUE;
}

BOOL DirectX12Engine::Shutdown()
{
	m_bRunning = false;

	m_MeshMap.clear();

	for (auto iter : m_ShaderProgramMap)
	{
		iter.second.PSByteCode.Reset();
		iter.second.VSByteCode.Reset();
	}

	m_pPSO.Reset();
	m_pRootSignature.Reset();
	m_pDepthStencilBuffer.Reset();
	m_pSwapChainBuffer[0].Reset();
	m_pSwapChainBuffer[1].Reset();
	m_pCbvHeap.Reset();
	m_pDsvHeap.Reset();
	m_pRtvHeap.Reset();
	m_pSwapChain.Reset();
	m_pCommandList.Reset();
	m_pCommandAllocator.Reset();
	m_pCommandQueue.Reset();
	m_pFence.Reset();
	m_pDevice.Reset();
	m_pDXGIFactory.Reset();
	m_pDebugController.Reset();

	return TRUE;
}

BOOL DirectX12Engine::Render()
{
	//XMVECTOR pos = XMVectorSet(4.21608206e-08, 3.53553391, -3.53553391, 1.0f);
	XMVECTOR pos = XMVectorSet(2, 6, -10, 1.0f);
	XMVECTOR target = XMVectorZero();
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	XMMATRIX view = XMMatrixLookAtLH(pos, target, up);
	
	XMMATRIX world = XMMatrixIdentity();
	XMMATRIX proj = XMMatrixPerspectiveFovLH(0.25f*3.1415926, AspectRatio(), 1.0f, 1000.0f);
	XMMATRIX worldViewProj = world*view*proj;

	ObjectConstants test;
	XMStoreFloat4x4(&test.WorldViewProj, XMMatrixTranspose(worldViewProj));
	//XMStoreFloat4x4(&test.WorldViewProj, worldViewProj);
	m_pObjectCB->CopyData(0, test);

	m_pCommandList->ClearDepthStencilView(GetDepthStencilView(), D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, nullptr);
	m_pCommandList->OMSetRenderTargets(1, &GetCurrentBackBufferView(), true, &GetDepthStencilView());

	for (auto iter : m_MeshMap)
	{
		ID3D12DescriptorHeap* descriptorHeaps[] = { m_pCbvHeap.Get() };
		m_pCommandList->SetDescriptorHeaps(_countof(descriptorHeaps), descriptorHeaps);

		m_pCommandList->SetGraphicsRootSignature(m_pRootSignature.Get());

		m_pCommandList->IASetVertexBuffers(0, 1, &iter.second->GetVertexBufferView());
		m_pCommandList->IASetIndexBuffer(&iter.second->GetIndexBufferView());
		m_pCommandList->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		m_pCommandList->SetGraphicsRootDescriptorTable(0, m_pCbvHeap->GetGPUDescriptorHandleForHeapStart());

		auto debug = iter.second->GetIndexCount();
		m_pCommandList->DrawIndexedInstanced(iter.second->GetIndexCount(), 1, 0, 0, 0);
	}



	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(GetCurrentBackBuffer(),
		D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));

	if (!ExecuteCommandLists())
		return FALSE;

	if (FAILED((m_pSwapChain->Present(0, 0))))
	{
		// Log
		return FALSE;
	}
	m_CurrentBackBuffer = (m_CurrentBackBuffer + 1) % m_GraphicsOptions.swapChainCount;

	if (!FlushCommandQueue())
		return FALSE;

	return TRUE;
}

BOOL DirectX12Engine::ClearScreen(const RGBAColor & clearColor)
{
	//if (FAILED(m_pCommandAllocator->Reset()))
	//{
	//	// Log
	//	return FALSE;
	//}
	if (FAILED(m_pCommandList->Reset(m_pCommandAllocator.Get(), m_pPSO.Get())))
	{
		// Log
		return FALSE;
	}

	m_pCommandList->RSSetViewports(1, &m_ScreenViewport);
	m_pCommandList->RSSetScissorRects(1, &m_ScissorRect);

	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(GetCurrentBackBuffer(),
		D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

	FLOAT rgba[4] = { clearColor.r, clearColor.g, clearColor.b, clearColor.a };
	
	m_pCommandList->ClearRenderTargetView(GetCurrentBackBufferView(), rgba, 0, nullptr);

	
	return TRUE;
}

BOOL DirectX12Engine::CreateShaderProgram(const char8 * pszProgramName)
{
	const std::string KEY = pszProgramName;

	ShaderProgramMap::iterator iter = m_ShaderProgramMap.find(KEY);
	if (iter != m_ShaderProgramMap.end())
	{
		// Log
		return FALSE;
	}

	ShaderProgram shaderProgram;
	m_ShaderProgramMap[KEY] = shaderProgram;

	return TRUE;
}

BOOL DirectX12Engine::DeleteShaderProgram(const char8 * pszProgramName)
{
	m_ShaderProgramMap.erase(std::string(pszProgramName));
	return TRUE;
}

BOOL DirectX12Engine::AttachShaderToProgram(const char8 * pszShaderProgramName, const char8 * pszProgramName, ShaderTypesEnum shaderType)
{
	const std::string KEY = pszProgramName;
	ShaderProgramMap::iterator iter = m_ShaderProgramMap.find(KEY);
	if (iter == m_ShaderProgramMap.end())
	{
		// Log
		return FALSE;
	}
	ShaderProgram* pProgram = &iter->second;

	std::string entryPoint;
	std::string compilerType;
	switch (shaderType)
	{
	case VertexShader:
		entryPoint = "VS";
		compilerType = "vs_5_1";
		break;
	case PixelShader:
		entryPoint = "PS";
		compilerType = "ps_5_1";
		break;
	}

	std::wstring filePathFORNOW = GApi::Util::Convert(pszShaderProgramName);
	Microsoft::WRL::ComPtr<ID3DBlob> byteCode;
	if (!Utility::CompileShader(filePathFORNOW, nullptr, entryPoint.c_str(), compilerType.c_str(), byteCode))
	{
		// Log
		return FALSE;
	}

	if (shaderType == VertexShader)
	{
		pProgram->VSByteCode = byteCode;
		pProgram->InputLayout =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			//{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
		};
	}
	else if (shaderType == PixelShader)
	{
		pProgram->PSByteCode = byteCode;
	}
	
	byteCode.Reset();
	byteCode = nullptr;
	
	return TRUE;
}

BOOL DirectX12Engine::RegisterMesh(Mesh & mesh, MeshID & meshID)
{
	if (FAILED(m_pCommandList->Reset(m_pCommandAllocator.Get(), nullptr)))
	{
		// Log
		return FALSE;
	}

	meshID = m_pMeshIDGenerator->GetNextId();
	DirectXMeshPtr pMesh = DirectXMeshPtr(new DirectXMesh(mesh));
	if (!pMesh->Initialize(m_pDevice.Get(), m_pCommandList.Get()))
		return FALSE;

	m_MeshMap[meshID] = pMesh;
	
	ExecuteCommandLists();

	return TRUE;
}

BOOL DirectX12Engine::DeregisterMesh(const MeshID meshID)
{
	DirectXMeshPtr pMesh = m_MeshMap[meshID];
	
	m_MeshMap.erase(meshID);
	m_pMeshIDGenerator->Release(meshID);

	auto meshToProgramIter = m_MeshToProgramMap.find(meshID);
	if (meshToProgramIter != m_MeshToProgramMap.end())
		m_MeshToProgramMap.erase(meshID);

	return 0;
}

BOOL DirectX12Engine::CopyMesh(const MeshID meshID, MeshID & newMesh)
{
	return 0;
}

BOOL DirectX12Engine::BindShaderToMesh(const char8 * pszProgramName, const MeshID meshID)
{
	const std::string KEY = pszProgramName;
	if (m_ShaderProgramMap.find(KEY) == m_ShaderProgramMap.end())
	{
		// Log
		return FALSE;
	}

	ShaderProgram shaderProgram = m_ShaderProgramMap[KEY];
	m_MeshToProgramMap[meshID] = shaderProgram;
	return TRUE;
}

BOOL DirectX12Engine::TransformMesh(const MeshID meshID, const Transform & transformation)
{
	return 0;
}

BOOL DirectX12Engine::RegisterTexture(std::string resourceName, TextureID & textureID)
{
	return 0;
}

BOOL DirectX12Engine::DeregisterTexture(const TextureID textureID)
{
	return 0;
}

BOOL DirectX12Engine::BindTextureToMesh(const TextureID textureID, const MeshID meshID)
{
	return 0;
}

CameraPtr DirectX12Engine::GetCamera()
{
	return m_pCamera;
}

inline std::string DirectX12Engine::GetEngineDescription() const
{
	return std::string("DirectX 12 Engine ALPHA");
}

BOOL DirectX12Engine::FlushCommandQueue()
{
	++m_CurrentFence;

	if (FAILED(m_pCommandQueue->Signal(m_pFence.Get(), m_CurrentFence)))
	{
		// Log
		return FALSE;
	}

	// Wait until the GPU has completed commands up to this fence point.
	if (m_pFence->GetCompletedValue() < m_CurrentFence)
	{
		HANDLE eventHandle = CreateEventEx(nullptr, false, false, EVENT_ALL_ACCESS);

		// Fire event when GPU hits current fence.  
		if (FAILED((m_pFence->SetEventOnCompletion(m_CurrentFence, eventHandle))))
		{
			// Log
			return FALSE;
		}

		// Wait until the GPU hits current fence event is fired.
		WaitForSingleObject(eventHandle, INFINITE);
		CloseHandle(eventHandle);
	}

	return TRUE;
}

BOOL DirectX12Engine::OnResize(const float32 width, const float32 height)
{
	if (!m_bRunning)
	{
		// Log warning
		return TRUE;
	}

	if (!FlushCommandQueue())
		return FALSE;

	if (FAILED(m_pCommandList->Reset(m_pCommandAllocator.Get(), nullptr)))
	{
		// Log
		return FALSE;
	}

	// Release the previous resources we will be recreating.
	for (int i = 0; i < m_GraphicsOptions.swapChainCount; ++i)
		m_pSwapChainBuffer[i].Reset();
	m_pDepthStencilBuffer.Reset();

	if (FAILED(m_pSwapChain->ResizeBuffers(
		m_GraphicsOptions.swapChainCount, 
		(UINT)width,
		(UINT)height,
		DXGI_FORMAT_R8G8B8A8_UNORM, 
		DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH)))
	{
		// Log
		return FALSE;
	}

	m_CurrentBackBuffer = 0;

	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHeapHandle(m_pRtvHeap->GetCPUDescriptorHandleForHeapStart());
	for (UINT i = 0; i < m_GraphicsOptions.swapChainCount; i++)
	{
		if (FAILED(m_pSwapChain->GetBuffer(i, IID_PPV_ARGS(&m_pSwapChainBuffer[i]))))
		{
			// Log
			return FALSE;
		}

		m_pDevice->CreateRenderTargetView(m_pSwapChainBuffer[i].Get(), nullptr, rtvHeapHandle);
		rtvHeapHandle.Offset(1, m_RtvDescriptorSize);
	}

	// Create the depth/stencil buffer and view.
	D3D12_RESOURCE_DESC depthStencilDesc;
	depthStencilDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	depthStencilDesc.Alignment = 0;
	depthStencilDesc.Width = (UINT64)width;
	depthStencilDesc.Height = (UINT64)height;
	depthStencilDesc.DepthOrArraySize = 1;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.SampleDesc.Count = 1;
	depthStencilDesc.SampleDesc.Quality = 0;
	depthStencilDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	depthStencilDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	D3D12_CLEAR_VALUE optClear;
	optClear.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	optClear.DepthStencil.Depth = 1.0f;
	optClear.DepthStencil.Stencil = 0;

	if (FAILED(m_pDevice->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&depthStencilDesc,
		D3D12_RESOURCE_STATE_COMMON,
		&optClear,
		IID_PPV_ARGS(m_pDepthStencilBuffer.GetAddressOf()))))
	{
		// Log
		return FALSE;
	}

	// Create descriptor to mip level 0 of entire resource using the format of the resource.
	m_pDevice->CreateDepthStencilView(m_pDepthStencilBuffer.Get(), nullptr, m_pDsvHeap->GetCPUDescriptorHandleForHeapStart());

	// Transition the resource from its initial state to be used as a depth buffer.
	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(m_pDepthStencilBuffer.Get(),
		D3D12_RESOURCE_STATE_COMMON, D3D12_RESOURCE_STATE_DEPTH_WRITE));

	// Execute the resize commands.
	if (!ExecuteCommandLists())
		return FALSE;

	// Wait until resize is complete.
	FlushCommandQueue();

	// Update the viewport transform to cover the client area.
	m_ScreenViewport.TopLeftX = 0;
	m_ScreenViewport.TopLeftY = 0;
	m_ScreenViewport.Width = static_cast<FLOAT>(width);
	m_ScreenViewport.Height = static_cast<FLOAT>(height);
	m_ScreenViewport.MinDepth = 0.0f;
	m_ScreenViewport.MaxDepth = 1.0f;

	m_ScissorRect = { 0, 0, (LONG)width, (LONG)height };

	return TRUE;
}

ID3D12Resource* DirectX12Engine::GetCurrentBackBuffer() const
{
	return m_pSwapChainBuffer[m_CurrentBackBuffer].Get();
}

D3D12_CPU_DESCRIPTOR_HANDLE DirectX12Engine::GetCurrentBackBufferView() const
{
	return CD3DX12_CPU_DESCRIPTOR_HANDLE(
		m_pRtvHeap->GetCPUDescriptorHandleForHeapStart(),
		m_CurrentBackBuffer,
		m_RtvDescriptorSize);
}

D3D12_CPU_DESCRIPTOR_HANDLE DirectX12Engine::GetDepthStencilView() const
{
	return m_pDsvHeap->GetCPUDescriptorHandleForHeapStart();
}

BOOL DirectX12Engine::CreateDXGIFactory()
{
	// Log

	if (FAILED(CreateDXGIFactory1(IID_PPV_ARGS(&m_pDXGIFactory))))
	{
		// Log failure
		return FALSE;
	}

	return TRUE;
}

BOOL DirectX12Engine::CreateDeviceAndFence()
{
	// Log

	if (FAILED(D3D12CreateDevice(nullptr, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&m_pDevice))))
	{
		// Log failure
		return FALSE;
	}
	
	if (FAILED(m_pDevice->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_pFence))))
	{
		// Log Failure
		return FALSE;
	}

	m_RtvDescriptorSize = m_pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	m_DsvDescriptorSize = m_pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
	m_CbvSrvDescriptorSize = m_pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	return TRUE;
}

BOOL DirectX12Engine::CheckMultisampleLevels(UINT& qualityLevels)
{
	// Log

	D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS multisampleQualityLevels;
	multisampleQualityLevels.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	multisampleQualityLevels.SampleCount = 4; // Configurable
	multisampleQualityLevels.Flags = D3D12_MULTISAMPLE_QUALITY_LEVELS_FLAG_NONE;
	multisampleQualityLevels.NumQualityLevels = 0;
	if (FAILED(m_pDevice->CheckFeatureSupport(D3D12_FEATURE_MULTISAMPLE_QUALITY_LEVELS, &multisampleQualityLevels, sizeof(D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS))))
	{
		// LOG
		return FALSE;
	}

	qualityLevels = multisampleQualityLevels.NumQualityLevels;
	return TRUE;
}

BOOL DirectX12Engine::CreateQueueAllocatorCommands()
{
	// Log

	D3D12_COMMAND_QUEUE_DESC queueDesc = {};
	queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	if (FAILED(m_pDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&m_pCommandQueue))))
	{
		// Log Failure
		return FALSE;
	}

	if (FAILED(m_pDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(m_pCommandAllocator.GetAddressOf()))))
	{
		// Log Failure
		return FALSE;
	}

	if (FAILED(m_pDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_pCommandAllocator.Get(), nullptr, IID_PPV_ARGS(m_pCommandList.GetAddressOf()))))
	{
		// Log Failure
		return FALSE;
	}

	m_pCommandList->Close();

	return TRUE;
}

BOOL DirectX12Engine::CreateSwapChain(const UINT qualityLevels)
{
	// Log
	m_pSwapChain.Reset();

	DXGI_SWAP_CHAIN_DESC sd;
	sd.BufferDesc.Width = (UINT)m_GraphicsOptions.width;
	sd.BufferDesc.Height = (UINT)m_GraphicsOptions.height;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = qualityLevels;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.BufferCount = (UINT)m_GraphicsOptions.swapChainCount;
	sd.OutputWindow = m_hWnd;
	sd.Windowed = true;
	sd.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	if (FAILED(m_pDXGIFactory->CreateSwapChain(m_pCommandQueue.Get(), &sd, m_pSwapChain.GetAddressOf())))
	{
		// Log Failure
		return FALSE;
	}

	return TRUE;
}

BOOL DirectX12Engine::CreateDescriptorHeaps()
{
	// Log

	D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc;
	rtvHeapDesc.NumDescriptors = m_GraphicsOptions.swapChainCount;
	rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	rtvHeapDesc.NodeMask = 0;
	if (FAILED(m_pDevice->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(m_pRtvHeap.GetAddressOf()))))
	{
		// Log
		return FALSE;
	}

	D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc;
	dsvHeapDesc.NumDescriptors = 1;
	dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	dsvHeapDesc.NodeMask = 0;
	if (FAILED(m_pDevice->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(m_pDsvHeap.GetAddressOf()))))
	{
		// Log
		return FALSE;
	}

	D3D12_DESCRIPTOR_HEAP_DESC cbvHeapDesc;
	cbvHeapDesc.NumDescriptors = 1;
	cbvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	cbvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbvHeapDesc.NodeMask = 0;
	if (FAILED((m_pDevice->CreateDescriptorHeap(&cbvHeapDesc, IID_PPV_ARGS(&m_pCbvHeap)))))
	{
		// Log
		return FALSE;
	}

	return TRUE;
}

BOOL DirectX12Engine::CreateConstantBuffers()
{
	m_pObjectCB = std::make_shared<UploadBuffer<ObjectConstants>>(m_pDevice.Get(), 1, true);
	if (!m_pObjectCB->Map())
	{
		// Log
		return FALSE;
	}
	D3D12_GPU_VIRTUAL_ADDRESS cbAddress = m_pObjectCB->GetResource()->GetGPUVirtualAddress();
	//int boxCBufIndex = 0;
	//cbAddress += boxCBufIndex*objCBByteSize;

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc;
	cbvDesc.BufferLocation = cbAddress;
	cbvDesc.SizeInBytes = Utility::CalcConstantBufferByteSize(sizeof(ObjectConstants));

	m_pDevice->CreateConstantBufferView(
		&cbvDesc,
		m_pCbvHeap->GetCPUDescriptorHandleForHeapStart());

	return TRUE;
}

BOOL DirectX12Engine::CreateRootSignature()
{
	// Root parameter can be a table, root descriptor or root constants.
	CD3DX12_ROOT_PARAMETER slotRootParameter[1];
	
	// Create a single descriptor table of CBVs.
	CD3DX12_DESCRIPTOR_RANGE cbvTable;
	cbvTable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
	slotRootParameter[0].InitAsDescriptorTable(1, &cbvTable);

	// A root signature is an array of root parameters.
	CD3DX12_ROOT_SIGNATURE_DESC rootSigDesc(1, slotRootParameter, 0, nullptr, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

	// create a root signature with a single slot which points to a descriptor range consisting of a single constant buffer
	ComPtr<ID3DBlob> serializedRootSig = nullptr;
	ComPtr<ID3DBlob> errorBlob = nullptr;
	if (FAILED(D3D12SerializeRootSignature(&rootSigDesc, D3D_ROOT_SIGNATURE_VERSION_1, serializedRootSig.GetAddressOf(), errorBlob.GetAddressOf())))
	{
		// Log
		return FALSE;
	}

	if (errorBlob != nullptr)
	{
		::OutputDebugStringA((char*)errorBlob->GetBufferPointer());
	}

	if (FAILED(m_pDevice->CreateRootSignature(
		0,
		serializedRootSig->GetBufferPointer(),
		serializedRootSig->GetBufferSize(),
		IID_PPV_ARGS(&m_pRootSignature))))
	{
		// Log
		return FALSE;
	}

	return TRUE;
}

BOOL DirectX12Engine::CreatePSO()
{

	ShaderProgram program = m_ShaderProgramMap["TestShader"];

	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc;
	ZeroMemory(&psoDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	psoDesc.InputLayout = { program.InputLayout.data(), (UINT)program.InputLayout.size() };
	psoDesc.pRootSignature = m_pRootSignature.Get();
	psoDesc.VS =
	{
		reinterpret_cast<BYTE*>(program.VSByteCode->GetBufferPointer()),
		program.VSByteCode->GetBufferSize()
	};
	psoDesc.PS =
	{
		reinterpret_cast<BYTE*>(program.PSByteCode->GetBufferPointer()),
		program.PSByteCode->GetBufferSize()
	};
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;
	psoDesc.SampleDesc.Quality =  0;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	if (FAILED(m_pDevice->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_pPSO))))
	{
		// Log
		return FALSE;
	}

	return TRUE;
}

BOOL DirectX12Engine::ExecuteCommandLists()
{
	if (FAILED(m_pCommandList->Close()))
	{
		// Log
		return FALSE;
	}

	ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
	m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

	return TRUE;
}