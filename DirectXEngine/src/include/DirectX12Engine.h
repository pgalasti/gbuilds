#ifndef DIRECT_X_12_ENGINE_H
#define DIRECT_X_12_ENGINE_H

#include "StdDirectXHeader.h"

#include "IGraphicsEngine.h"

#include <unordered_map>

#include <windows.h>
#include <wrl/client.h>
#include <dxgi1_5.h>
#include <d3d12.h>
#include <D3Dcompiler.h>
#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <DirectXColors.h>
#include <DirectXCollision.h>
#include "IDGenerator.h"
#include "DirectXMesh.h"
#include "ShaderProgramStruct.h"
#include "UploadBuffer.h"

using namespace DirectX;

namespace GApi {
	namespace DirectX {
		namespace Engine {

			struct ObjectConstants
			{
				XMFLOAT4X4 WorldViewProj = XMFLOAT4X4(
					1.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 1.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 1.0f);
			};

			using GApi::Graphics::IGraphicsEngine;
			using namespace GApi::Graphics;
			using namespace GApi::DirectX::Structure;

			typedef std::unordered_map<MeshID, DirectXMeshPtr> MeshMap;
			typedef std::unordered_map<std::string, ShaderProgram> ShaderProgramMap;
			typedef std::unordered_map<MeshID, ShaderProgram> MeshToProgramMap;

			class DllExport DirectX12Engine : implements IGraphicsEngine
			{
			public:
				DirectX12Engine();
				virtual ~DirectX12Engine() 
				{
					SAFE_DELETE(m_pMeshIDGenerator);
				}
				
				// Window Management
				virtual BOOL OnResize(const float32 width, const float32 height) override;

				// Options
				virtual BOOL SetOptions(const GraphicsEngineOptions& options) override;
				virtual BOOL SetWindowHandle(void* ptr) override;

				// Initialize/Shutdown
				virtual BOOL Startup() override;
				virtual BOOL Shutdown() override;

				// Rendering
				virtual BOOL Render() override;
				virtual BOOL ClearScreen(const RGBAColor& clearColor) override;

				// Shader Program Functions
				virtual BOOL CreateShaderProgram(const char8* pszProgramName) override;
				virtual BOOL DeleteShaderProgram(const char8* pszProgramName) override;
				virtual BOOL AttachShaderToProgram(const char8* pszShaderProgramName, const char8* pszProgramName, ShaderTypesEnum shaderType) override;

				// Modeling
				virtual BOOL RegisterMesh(Mesh& mesh, MeshID& meshID) override;
				virtual BOOL DeregisterMesh(const MeshID meshID) override;
				virtual BOOL CopyMesh(const MeshID meshID, MeshID& newMesh) override;
				virtual BOOL BindShaderToMesh(const char8* pszProgramName, const MeshID meshID) override;
				virtual BOOL TransformMesh(const MeshID meshID, const Transform& transformation) override;

				// Texture
				virtual BOOL RegisterTexture(std::string resourceName, TextureID& textureID) override;
				virtual BOOL DeregisterTexture(const TextureID textureID) override;
				virtual BOOL BindTextureToMesh(const TextureID textureID, const MeshID meshID) override;

				// Camera
				virtual CameraPtr GetCamera() override;

				inline virtual std::string GetEngineDescription() const override;

			private:
				GraphicsEngineOptions m_GraphicsOptions;

				CameraPtr m_pCamera;
				bool m_bRunning;

				HWND m_hWnd;

				// Engine Management
				IDGenerator<MeshID>* m_pMeshIDGenerator;
				MeshMap m_MeshMap;
				ShaderProgramMap m_ShaderProgramMap;
				MeshToProgramMap m_MeshToProgramMap;

				// DirectX Internals
				Microsoft::WRL::ComPtr<IDXGIFactory5> m_pDXGIFactory;
				Microsoft::WRL::ComPtr<ID3D12Device> m_pDevice;
				Microsoft::WRL::ComPtr<ID3D12Fence> m_pFence;
				Microsoft::WRL::ComPtr<ID3D12CommandQueue> m_pCommandQueue;
				Microsoft::WRL::ComPtr<ID3D12CommandAllocator> m_pCommandAllocator;
				Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> m_pCommandList;
				Microsoft::WRL::ComPtr<IDXGISwapChain> m_pSwapChain;
				Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_pRtvHeap;
				Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_pDsvHeap;
				Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_pCbvHeap;
				Microsoft::WRL::ComPtr<ID3D12Resource> m_pSwapChainBuffer[2];
				Microsoft::WRL::ComPtr<ID3D12Resource> m_pDepthStencilBuffer;
				Microsoft::WRL::ComPtr<ID3D12RootSignature> m_pRootSignature;
				Microsoft::WRL::ComPtr<ID3D12PipelineState> m_pPSO;
#ifdef GAPI_DEBUG
				Microsoft::WRL::ComPtr<ID3D12Debug> m_pDebugController;
#endif
				ubyte8 m_CurrentBackBuffer;

				D3D12_VIEWPORT m_ScreenViewport;
				D3D12_RECT m_ScissorRect;

				UINT m_RtvDescriptorSize;
				UINT m_DsvDescriptorSize;
				UINT m_CbvSrvDescriptorSize;

				uint64 m_CurrentFence;

				std::shared_ptr<UploadBuffer<ObjectConstants>> m_pObjectCB = nullptr;

				BOOL FlushCommandQueue();

				ID3D12Resource* GetCurrentBackBuffer() const;
				D3D12_CPU_DESCRIPTOR_HANDLE GetCurrentBackBufferView() const;
				D3D12_CPU_DESCRIPTOR_HANDLE GetDepthStencilView() const;

				// Initialization
				BOOL CreateDXGIFactory();
				BOOL CreateDeviceAndFence();
				BOOL CheckMultisampleLevels(UINT& qualityLevels);
				BOOL CreateQueueAllocatorCommands();
				BOOL CreateSwapChain(const UINT qualityLevels);
				BOOL CreateDescriptorHeaps();
				BOOL CreateConstantBuffers();
				BOOL CreateRootSignature();
				BOOL CreatePSO();
				
				float32 AspectRatio() const { return m_GraphicsOptions.width / m_GraphicsOptions.height;  }

				// Invoke Methods
				BOOL ExecuteCommandLists();
			};

		}
	}
}

#endif
