#ifndef GAPI_UPLOAD_BUFFER_H
#define GAPI_UPLOAD_BUFFER_H

#include "StdDirectXHeader.h"
#include <wrl/client.h>
//#include "d3dx12.h"
#include "D3DUtil.h"

using Microsoft::WRL::ComPtr;

namespace GApi {
	namespace DirectX {
		namespace Structure {

			template<typename T>
			class UploadBuffer
			{
			public:
				DISABLE_COPY_CONSTRUCTOR(UploadBuffer);
				DISABLE_ASSIGNMENT_OPERATOR(UploadBuffer);

				UploadBuffer(ID3D12Device* pDevice, uint32 elementCount, bool bConstantBuffer = true) 
					: m_bConstantBuffer(bConstantBuffer), m_ElementCount(elementCount), m_MappedData(nullptr)
				{
					m_ElementByteSize = sizeof(T);

					if (m_bConstantBuffer)
						m_ElementByteSize = Utility::CalcConstantBufferByteSize(m_ElementByteSize);

					m_pDevice = pDevice;
				}

				~UploadBuffer()
				{
					this->UnMap();
				}

				void UnMap()
				{
					if (IsNotNull(m_pUploadBuffer))
						m_pUploadBuffer->Unmap(0, nullptr);

					m_pUploadBuffer.Reset();
					m_MappedData = nullptr;
				}

				BOOL Map()
				{
					if (FAILED(m_pDevice->CreateCommittedResource(
						&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
						D3D12_HEAP_FLAG_NONE,
						&CD3DX12_RESOURCE_DESC::Buffer(m_ElementByteSize*m_ElementCount),
						D3D12_RESOURCE_STATE_GENERIC_READ,
						nullptr,
						IID_PPV_ARGS(&m_pUploadBuffer))))
					{
						// Log
						return FALSE;
					}

					if (FAILED(m_pUploadBuffer->Map(0, nullptr, reinterpret_cast<void**>(&m_MappedData))))
					{
						// Log
						return FALSE;
					}


					return TRUE;
				}

				ID3D12Resource* GetResource() const { return m_pUploadBuffer.Get(); }
				
				void CopyData(uint32 elementIndex, const T& data)
				{
					memcpy(&m_MappedData[elementIndex*m_ElementByteSize], &data, sizeof(T));
				}

			private:
				bool m_bConstantBuffer;
				
				uint32 m_ElementByteSize;
				uint32 m_ElementCount;

				sbyte8* m_MappedData;

				ID3D12Device* m_pDevice;
				ComPtr<ID3D12Resource> m_pUploadBuffer;
			};
		}
	}
}

#endif
