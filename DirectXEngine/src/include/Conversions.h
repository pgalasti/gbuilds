#ifndef CONVERSIONS_H
#define CONVERSIONS_H

#include "StdDirectXHeader.h"

#include <DirectXMath.h>


#include "GVector.h"
#include "GPoint.h"

using namespace DirectX;
using namespace GApi::GMath;

namespace GApi {
	namespace DirectX {
		namespace Math {

			const XMFLOAT2 ConvertToXMFloat(const Vector2D& vector);
			const XMFLOAT3 ConvertToXMFloat(const Vector3D& vector);

			const XMFLOAT2 ConvertToXMFloat(const Point2D<float32>& point);
			const XMFLOAT3 ConvertToXMFloat(const Point3D<float32>& point);
			const XMFLOAT4 ConvertToXMFloat(const Point4D<float32>& point);

			const XMVECTOR ConvertToXMVector(const Vector2D& vector);
			const XMVECTOR ConvertToXMVector(const Vector3D& vector);

		}
	}
}


#endif
