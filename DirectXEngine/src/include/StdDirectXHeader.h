#ifndef STD_DIRECTX_HEADER
#define STD_DIRECTX_HEADER

#include "StdHeader.h"

#ifdef _WIN32

#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "D3D12.lib")
#pragma comment(lib, "dxgi.lib")

#endif

#endif
