#ifndef GAPI_DIRECTX_MESH_H
#define GAPI_DIRECTX_MESH_H

#include "StdDirectXHeader.h"

#include "GMesh.h"
#include "d3d12.h"
#include "d3dcompiler.h"
#include <wrl/client.h>
#include <memory>

using namespace GApi::Graphics;

namespace GApi {
	namespace DirectX {
		namespace Engine {

			class DirectXMesh
			{
			public:

				DISABLE_IMPLICIT_CONSTRUCTOR(DirectXMesh)

				DirectXMesh(Mesh& mesh);
				~DirectXMesh();

				BOOL Initialize(ID3D12Device* pDevice, ID3D12GraphicsCommandList* pCommandList);
				D3D12_VERTEX_BUFFER_VIEW GetVertexBufferView() const;
				D3D12_INDEX_BUFFER_VIEW GetIndexBufferView() const;

				const uint32 GetIndexCount() const { return m_IndexCount; }

			private:

				uint32 m_VertexCount;
				uint32 m_IndexCount;

				uint32 m_VertexByteStride;

				uint32 m_VertexByteSize;
				uint32 m_IndexByteSize;

				Vertex* m_pVerticies;
				ushort16* m_pIndicies;

				Microsoft::WRL::ComPtr<ID3DBlob> m_pVertexBufferCPU;
				Microsoft::WRL::ComPtr<ID3DBlob> m_pIndexBufferCPU;

				Microsoft::WRL::ComPtr<ID3D12Resource> m_pVertexBufferGPU;
				Microsoft::WRL::ComPtr<ID3D12Resource> m_pVertexUploadBufferGPU;

				Microsoft::WRL::ComPtr<ID3D12Resource> m_pIndexBufferGPU;
				Microsoft::WRL::ComPtr<ID3D12Resource> m_pIndexUploadBufferGPU;
			};

			typedef std::shared_ptr<DirectXMesh> DirectXMeshPtr;

		}
	}
}


#endif
