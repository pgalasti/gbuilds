#ifndef WIN32IMPL_H
#define WIN32IMPL_H

#include "StdDirectXHeader.h"

#include "IGameWindow.h"
#include "IGameEngine.h"
#include "GameWindowImpl.h"

#include <WindowsX.h>
#include <Windows.h>

using GApi::UI::WindowOptions;
using GApi::UI::IGameWindow;
using GApi::UI::GameWindowImpl;
using GApi::GameEngine::IGameEngine;

namespace GApi {
	namespace DirectX {
		namespace Window {

			class DllExport Win32GameWindow : public GameWindowImpl
			{
			public:
				Win32GameWindow(HINSTANCE hInstance);
				virtual ~Win32GameWindow();

				virtual BOOL Initialize(const WindowOptions& options) override;
				virtual BOOL Cleanup() override;
				virtual BOOL Start();
				virtual BOOL Run(const float32 deltaTime) override;

				virtual inline void SetWindowTitle(const char8* pszName) override;
				virtual inline BOOL SetGameEngine(IGameEngine* pEngine) override;
				virtual BOOL GetWindowHandle(void* ptr) override;
				
				
				LRESULT MsgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
				static Win32GameWindow* GetStaticInstance();

			private:

				WindowOptions m_WindowOptions;

				HWND m_hMainWnd;
				HINSTANCE m_hAppInstance;
				bool m_Resizing = false;

				
			};

		}
	}
}

#endif