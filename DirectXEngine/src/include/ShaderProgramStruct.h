#ifndef GAPI_DIRECTX_SHADER_PROG_H
#define GAPI_DIRECTX_SHADER_PROG_H

#include <wrl/client.h>
#include <d3d12.h>

#include <vector>

using Microsoft::WRL::ComPtr;

namespace GApi {
	namespace DirectX {
		namespace Structure {

			struct ShaderProgram
			{
				ComPtr<ID3DBlob> VSByteCode;
				ComPtr<ID3DBlob> PSByteCode;
				std::vector<D3D12_INPUT_ELEMENT_DESC> InputLayout;
			};

		}
	}
}

#endif
