#include "StdDirectXHeader.h"

#include "Conversions.h"

using namespace DirectX;
using namespace GApi::GMath;

const XMFLOAT2 GApi::DirectX::Math::ConvertToXMFloat(const Vector2D & vector)
{
	XMFLOAT2 xmFloat2;
	xmFloat2.x = vector.xf;
	xmFloat2.y = vector.yf;

	return xmFloat2;
}

const XMFLOAT3 GApi::DirectX::Math::ConvertToXMFloat(const Vector3D & vector)
{
	XMFLOAT3 xmFloat3;
	xmFloat3.x = vector.xf;
	xmFloat3.y = vector.yf;
	xmFloat3.z = vector.zf;

	return xmFloat3;
}

const XMFLOAT2 GApi::DirectX::Math::ConvertToXMFloat(const Point2D<float32>& point)
{
	XMFLOAT2 xmFloat2;
	xmFloat2.x = point.x;
	xmFloat2.y = point.y;

	return xmFloat2;
}

const XMFLOAT3 GApi::DirectX::Math::ConvertToXMFloat(const Point3D<float32>& point)
{
	XMFLOAT3 xmFloat3;
	xmFloat3.x = point.x;
	xmFloat3.y = point.y;
	xmFloat3.z = point.z;

	return xmFloat3;
}

const XMFLOAT4 GApi::DirectX::Math::ConvertToXMFloat(const Point4D<float32>& point)
{
	XMFLOAT4 xmFloat4;
	xmFloat4.x = point.x;
	xmFloat4.y = point.y;
	xmFloat4.z = point.z;
	xmFloat4.w = point.w;

	return xmFloat4;
}

const XMVECTOR GApi::DirectX::Math::ConvertToXMVector(const Vector2D & vector)
{
	XMFLOAT2 xmFloat2 = ConvertToXMFloat(vector);

	return XMLoadFloat2(&xmFloat2);
}

const XMVECTOR GApi::DirectX::Math::ConvertToXMVector(const Vector3D & vector)
{
	XMFLOAT3 xmFloat3 = ConvertToXMFloat(vector);

	return XMLoadFloat3(&xmFloat3);
}
